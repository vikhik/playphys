-- premake5.lua

-- TODO LIST
-- Build glfw and gtest from source
-- RunCodeAnalysis/CAExcludePath
-- EnableMicrosoftCodeAnalysis=false
-- EnableCppCoreCheck=false
-- EnableClangTidyCodeAnalysis=true
-- SDLCheck
-- examine using Requires for internal projects

-- Clean Function --
newaction {
    trigger     = "clean",
    description = "clean the generated and built files",
    execute     = function ()
       print("cleaning the premake files...")
       os.rmdir("./Generated")
       print("cleaning the build files...")
       os.rmdir("./Build")
       print("done.")
    end
}

workspace "PlayPhys"
    -- Configurations
    -- Optimisations, Symbols, Defines, etc.
    configurations { "Debug", "DebugOpt", "Release", "Final" }
    filter { "configurations:Debug" }
        defines { "DEBUG" }
        symbols "On"
    filter { "configurations:DebugOpt" }
        defines { "DEBUG" }
        optimize "Debug"
        symbols "On"
    filter { "configurations:Release" }
        defines { "NDEBUG", "FORCE_PROFILING=true" }
        optimize "On"
        symbols "On"
        intrinsics "On"
        functionlevellinking "On"
        flags { "LinkTimeOptimization" }
    filter { "configurations:Final" }
        defines { "NDEBUG" }
        optimize "Full"
        intrinsics "On"
        functionlevellinking "On"
        flags { "LinkTimeOptimization" }
    filter { }

    -- Platforms
    -- Shorthand for toolset & architecture - displayed in the IDE
    platforms { "msvc32", "msvc64", "clang32", "clang64" }
    filter { "platforms:msvc32" }
        architecture "x86"
        toolset "msc"
    filter { "platforms:msvc64" }
        architecture "x86_64"
        toolset "msc"
    filter { "platforms:clang32" }
        architecture "x86"
        toolset "clang"
    filter { "platforms:clang64" }
        architecture "x86_64"
        toolset "clang"
    filter { }

    -- C++ Compiler Settings and Defines
    cppdialect "C++20"
    warnings "Extra"
    flags { "FatalWarnings", "MultiProcessorCompile" } -- "LinkTimeOptimization", 
    staticruntime "Off" -- i.e. MultiThreadedDLL
    filter { "toolset:clang" }
        defines { "PP_CLANG" }
        buildoptions { "-Wno-unused-function", "-Wno-microsoft-include", "-Wno-error=unused-parameter" }
    filter { "toolset:msc" }
        defines { "PP_MSVC" }
        disablewarnings { "4201", "4505", "4100" }
    filter { }

    -- Folder Structure
	location "Generated"
    objdir "Build/obj/%{cfg.platform}/%{cfg.buildcfg}/%{prj.name}"
    targetdir "Build/bin/%{cfg.platform}/%{cfg.buildcfg}"
    includedirs {"./Third-Party/Include" }
    

    -- TODO remove all use of configurations:Debug or DebugOpt
    -- when we build third party and replace with %{cfg.buildcfg}
    filter { "architecture:x86", "configurations:Debug or DebugOpt" }
        libdirs {"./Third-Party/Lib32/Debug" }
    filter { "architecture:x86", "configurations:Release or Final" }
        libdirs {"./Third-Party/Lib32/Release" }
    filter  { "architecture:x86_64", "configurations:Debug or DebugOpt" }
        libdirs {"./Third-Party/Lib/Debug" }
    filter  { "architecture:x86_64", "configurations:Release or Final" }
        libdirs {"./Third-Party/Lib/Release" }
    filter { }

    startproject "Example"


-- https://premake.github.io/docs/Sharing-Configuration-Settings

function usesVulkan()
    local vulkanDir = os.getenv('VULKAN_SDK')

    if vulkanDir == "" then
        error "could not find vulkan dir, please set the env var VULKAN_SDK"
    end

    includedirs { "%{os.getenv('VULKAN_SDK')}/Include", "%{os.getenv('VULKAN_SDK')}/Third-Party/Include" }

    filter { "architecture:x86" }
        libdirs {"%{os.getenv('VULKAN_SDK')}/Lib32" }
    filter  { "architecture:x86_64" }
        libdirs {"%{os.getenv('VULKAN_SDK')}/Lib" }
    filter { }

    links { "vulkan-1" }
end

function usesGLFW()
    filter { "system:windows" }
        defines { "WIN32" }
    filter { }

    links { "glfw3dll" }
end

function usesPlayPhys()
    kind "ConsoleApp"
    language "C++"
    files { "%{prj.name}/**.h", "%{prj.name}/**.c", "%{prj.name}/**.cpp" }
    links { "PlayPhys" }
    includedirs {"./PlayPhys/Source" }
    defines { "PP_DYNAMIC_LIB" }
    
    filter { "architecture:x86", "configurations:Debug or DebugOpt" }
        prebuildmessage ("{COPYDIR} ../Third-Party/Lib32/Debug/ %{cfg.targetdir}")
        prebuildcommands ("{COPYDIR} ../Third-Party/Lib32/Debug/ %{cfg.targetdir}")
    filter { "architecture:x86", "configurations:Release or Final" }
        prebuildmessage ("{COPYDIR} ../Third-Party/Lib32/Release/ %{cfg.targetdir}")
        prebuildcommands ("{COPYDIR} ../Third-Party/Lib32/Release/ %{cfg.targetdir}")
    filter  { "architecture:x86_64", "configurations:Debug or DebugOpt" }
        prebuildmessage ("{COPYDIR} ../Third-Party/Lib/Debug/ %{cfg.targetdir}")
        prebuildcommands ("{COPYDIR} ../Third-Party/Lib/Debug/ %{cfg.targetdir}")
    filter  { "architecture:x86_64", "configurations:Release or Final" }
        prebuildmessage ("{COPYDIR} ../Third-Party/Lib/Release/ %{cfg.targetdir}")
        prebuildcommands ("{COPYDIR} ../Third-Party/Lib/Release/ %{cfg.targetdir}")
    filter { }

    usesVulkan()
    usesGLFW()
end

-- TODO: This doesn't seem to quite work, bleh.
-- If it did, we could just make this a rule inside the game or playphys, rather than shaders being a separate project
-- rule "CompileVulkanShaders"
--     display "glslang compilation of shaders"
--     location "Generated"
--     fileextension ".vert"
--     -- Imitates the internal shader path in the output dir
--     -- '..\Shaders\Source\a\b\c\triangle.frag' -> 'Source\a\b\c\''
--     buildmessage 'Compiling Shader from %{prj.location} using: glslangValidator -V -o "%{cfg.targetdir}/%{path.getrelative("../Shaders/Source/", file.relpath)}.spv" "%{file.relpath}"'
--     buildcommands {
--         'glslangValidator -V -o "%{cfg.targetdir}/%{path.getrelative("../Shaders/Source/", file.relpath)}.spv" "%{file.relpath}"'
--     }
--     buildoutputs { '%{cfg.targetdir}/%{path.getrelative("../Shaders/Source/", file.relpath)}.spv' }

project "Shaders"
    kind "Utility"
    -- TODO: "**/*.frag;**/*.vert;**/*.tesc;**/*.tese;**/*.geom;**/*.comp"
    files { "Shaders/**.vert", "Shaders/**.frag", "Shaders/**.geom", "Shaders/**.comp", "Shaders/**.tesc", "Shaders/**.tese" }
    -- rules { "CompileVulkanShaders" }

    -- TODO: "**/*.frag;**/*.vert;**/*.tesc;**/*.tese;**/*.geom;**/*.comp"
    filter 'files:**'
        -- Imitates the internal shader path in the output dir
        -- '..\Shaders\Source\a\b\c\triangle.frag' -> 'Source\a\b\c\''
        buildmessage 'Compiling Shader from %{prj.location} using: glslangValidator -V -o "%{cfg.targetdir}/%{path.getrelative("../Shaders/Source/", file.relpath)}.spv" "%{file.relpath}"'
        buildcommands {
            'glslangValidator -V -o "%{cfg.targetdir}/%{path.getrelative("../Shaders/Source/", file.relpath)}.spv" "%{file.relpath}"'
        }
        buildoutputs { '%{cfg.targetdir}/%{path.getrelative("../Shaders/Source/", file.relpath)}.spv' }
    filter { }

project "PlayPhys"
    kind "SharedLib"
    language "C++"
    files { "%{prj.name}/**.h", "%{prj.name}/**.c", "%{prj.name}/**.cpp" }
    includedirs {"./PlayPhys/Source" }
    defines { "PP_DYNAMIC_LIB" }

    pchheader "enginepch.h"
    pchsource "%{prj.name}/Source/enginepch.cpp"

    dependson { "Shaders" }

    -- TODO: Customisability based on kwargs if I ever implement another system
    usesVulkan()
    usesGLFW()

project "Example"
    usesPlayPhys()

project "UnitTests"
    usesPlayPhys()
    filter { "configurations:Debug or DebugOpt" }
        links { "gtestd" }
    filter { "configurations:Release or Final" }
        links { "gtest" }
