#pragma once

#include <iterator>
#include <fstream>
#include <filesystem>
#include <string>

namespace PP
{
	namespace Utils
	{
		namespace Filesystem
		{
			using namespace std;

			static void setCwd(filesystem::path path)
			{
				filesystem::current_path(path);
			}

			static filesystem::path getCwd()
			{
				return filesystem::current_path();
			}

			// Reset the cwd to be the executable dir
			// TODO: Verify this works non-MSVC/non-Windows
			static void resetCwd(std::string_view argv0)
			{
				auto basePath = argv0.substr(0, argv0.find_last_of("/"));

				if (basePath.length() == argv0.length())
				{
					basePath = argv0.substr(0, argv0.find_last_of("\\"));
				}

				setCwd(basePath);
			}

			static std::vector<char> readFile(const std::string& filename)
			{
				// start reading at end of file, as a binary file
				std::ifstream file(filename, std::ios::ate | std::ios::binary);

				if (!file.is_open())
				{
					// FIXME: Figure out why this doesn't auto-breakpoint
					throw std::runtime_error("failed to open file!");
				}

				// because we are at the end of the file, we can allocate the correct size
				const auto fileSize = static_cast<size_t>(file.tellg());
				std::vector<char> buffer(fileSize);

				// seek back to the 0th byte, and read it into the buffer
				file.seekg(0);
				file.read(buffer.data(), fileSize);

				file.close();
				return buffer;
			}
		}

		namespace Collections
		{
			// from https://stackoverflow.com/questions/5056645/sorting-stdmap-using-value

			// Swaps pairs
			template<typename A, typename B>
			static std::pair<B, A> flipPair(const std::pair<A, B>& pair)
			{
				return std::pair<B, A>(pair.second, pair.first);
			}

			// Flips a map to be by value, rather than by key
			template<typename A, typename B>
			static std::multimap<B, A> flipMap(const std::map<A, B>& source)
			{
				std::multimap<B, A> result;

				std::transform(
					source.begin(), source.end(),
					std::inserter(result, result.begin()),
					flipPair<A, B>);

				return result;
			}
		}

		namespace CLI
		{
			static void waitForEnter()
			{
				std::cin.ignore(std::cin.rdbuf()->in_avail());
				std::cout << std::endl << "Press [ENTER] to continue.\n" << std::endl;
				std::cin.ignore();
			}

			static std::vector<std::string_view> readArgs(const int argc, const char** argv)
			{
				std::vector<std::string_view> args { static_cast<size_t>(argc) };
				for (int i = 0; i < argc; ++i)
				{
					args[i] = argv[i];
				}
				return args;
			}
		}
	}
}