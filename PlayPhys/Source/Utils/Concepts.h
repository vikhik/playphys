#pragma once

namespace PP::Concepts
{
#if defined(__cpp_lib_concepts)

#include <concepts>
	template<class T>
	concept Numeric = std::is_integral_v<T> || std::is_floating_point_v<T>;

#else

#define Numeric typename

#endif

}
