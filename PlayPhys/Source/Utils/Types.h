//
// Inspired by Handmade Hero / Casey Muratori
// 
// To improve control/portability of types
//

#pragma once

#include <cstdint>
#include <cstdlib>

namespace PP
{
	using size_t = std::size_t;

	using int8 = int8_t;
	using int16 = int16_t;
	using int32 = int32_t;
	using int64 = int64_t;

	using uint8 = uint8_t;
	using uint16 = uint16_t;
	using uint32 = uint32_t;
	using uint64 = uint64_t;

	using real32 = float;
	using real64 = double;

	using bool8 = uint8;
	using bool32 = uint32;
	// https://hero.handmade.network/forums/code-discussion/t/41-why_use_bool32_in_stead_of_bool#2855

	// By using uint8 instead of bool as the typedef, this is guaranteed to be true
	static_assert(sizeof(bool8) == 1, "bool8 expected to be 1 byte...");
}