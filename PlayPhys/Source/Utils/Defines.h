#pragma once

#include <cassert>

// TODO: use something other than integers for our defines (Platform/Graphics System)
// static constexpr something?

/* Configuration macros. Set in user code/props/CLI, or here for testing. */
#if !defined(FORCE_PROFILING)
#define FORCE_PROFILING false
#endif

/* Utility macros */
#define PP_NOT_IMPLEMENTED static_assert(false, "Not yet implemented")

/* API */
#if defined(PP_DYNAMIC_LIB)
#define PP_API __declspec(dllexport)
#else
#define PP_API __declspec(dllimport)
#endif

/* Conditional macros - automatic, do not touch. */
#if !defined(PP_PROFILER_ENABLED) && (!defined(NDEBUG) || FORCE_PROFILING == true)
#define PP_PROFILER_ENABLED true
#else
#define PP_PROFILER_ENABLED false
#endif

#if defined(PP_MSVC)
#define PP_COMPILER_MSVC true
#define PP_COMPILER_CLANG false
#elif defined(PP_CLANG)
#define PP_COMPILER_CLANG true
#define PP_COMPILER_MSVC false
#endif


/* Platform system */
#define PP_PLATFORM_GLFW 0

#if !defined(PP_PLATFORM_SYSTEM)
#define PP_PLATFORM_SYSTEM PP_PLATFORM_GLFW
#endif

/* Graphics system */
#define PP_GRAPHICS_VULKAN 0
//#define PP_GRAPHICS_OPENGL 1
//#define PP_GRAPHICS_DX11 2
//#define PP_GRAPHICS_DX12 3

#if !defined(PP_GRAPHICS_SYSTEM)
#define PP_GRAPHICS_SYSTEM PP_GRAPHICS_VULKAN
#endif

#if PP_GRAPHICS_SYSTEM == PP_GRAPHICS_VULKAN
#define GLFW_INCLUDE_VULKAN true
#endif

#if defined(NDEBUG) && !defined (PP_GRAPHICS_VALIDATION)
#define PP_GRAPHICS_VALIDATION false
#else
#define PP_GRAPHICS_VALIDATION true
#endif