#pragma once

#include <chrono>
#include <string>
#include <memory>

#include "Utils/Defines.h"

#if PP_PROFILER_ENABLED

#include <map>

namespace PP
{
	struct Profiler
	{
	public:
		PP_API Profiler(std::string domain, std::string section);

		Profiler(const Profiler&) = delete;
		Profiler(Profiler&&) = delete;
		Profiler& operator=(const Profiler&) = delete;
		Profiler& operator=(Profiler&&) = delete;

		PP_API ~Profiler();

		const std::string domain;
		const std::string section;

	private:
		std::chrono::time_point<std::chrono::high_resolution_clock> start_time;
	};

	/* ProfilerManager
	 * 
	 * Singleton
	 **/
	class ProfilerManager
	{
	public:
		PP_API static ProfilerManager* getInstance() noexcept
		{
			static std::unique_ptr<ProfilerManager> instance = std::make_unique<ProfilerManager>(ProfilerManager());

			return instance.get();
		}

		PP_API std::chrono::nanoseconds getTime(std::string domain, std::string section = "")
		{
			auto completed_profiles = getCompletedProfiles();
			if (completed_profiles.find(domain) == completed_profiles.end())
			{
				return {};
			}
			auto& domain_map = completed_profiles[domain];

			if (section.empty())
			{
				std::chrono::nanoseconds total{};
				for (auto& section_pair : domain_map)
					total += section_pair.second;
				return total;
			}

			if (domain_map.find(section) == domain_map.end())
			{
				return {};
			}

			return domain_map[section];
		}

	protected:
		friend struct Profiler;

		void createProfile(std::string domain, std::string section)
		{
			auto& completed_profiles = profiles;
			if (completed_profiles.find(domain) == completed_profiles.end())
			{
				completed_profiles[domain] = std::map<std::string, std::chrono::nanoseconds>();
			}

			auto& domain_map = completed_profiles[domain];

			if (domain_map.find(section) == domain_map.end())
			{
				domain_map[section] = { };
			}
		}

		void logProfiler(const Profiler& profiler, std::chrono::nanoseconds duration) noexcept
		{
			auto& domain_map = profiles[profiler.domain];

			domain_map[profiler.section] += duration;
		}

		// { Domain : { Section : Total Time } }
		const std::map<std::string, std::map<std::string, std::chrono::nanoseconds>>& getCompletedProfiles() const noexcept
		{
			return profiles;
		}

	private:
		ProfilerManager() = default;

		std::map<std::string, std::map<std::string, std::chrono::nanoseconds>> profiles{};

		PP_API friend std::ostream& operator<<(std::ostream& os, const ProfilerManager& pm);

		static void outputSection(std::ostream& os, std::chrono::nanoseconds duration_ns, std::string section);
	};
}

#elif !PP_PROFILER_ENABLED

namespace PP
{
	class ProfilerManager
	{
	public:
		PP_API static ProfilerManager* getInstance() noexcept
		{
			return nullptr;
		}

		PP_API std::chrono::nanoseconds getTime(std::string domain, std::string section = "")
		{
			return {};
		}

	private:
		ProfilerManager() = default;

		friend std::ostream& operator<<(std::ostream& os, const ProfilerManager& pm)
		{
			return os << "Profiler disabled unless PP_PROFILER_ENABLED" << std::endl;
		}
	};
}

#endif
