#include "enginepch.h"

#if PP_PROFILER_ENABLED

#include "Profiler.h"

namespace PP
{
	Profiler::Profiler(std::string domain, std::string section) : domain(domain), section(section)
	{
		// Create the domain/section here, before we log start_time?
		// that can remove branching from the destructor?
		ProfilerManager::getInstance()->createProfile(domain, section);

		start_time = std::chrono::high_resolution_clock::now();
	};

	Profiler::~Profiler()
	{
		const auto end_time = std::chrono::high_resolution_clock::now();
		const auto duration_ns = std::chrono::duration_cast<std::chrono::nanoseconds>(end_time - start_time);

		ProfilerManager::getInstance()->logProfiler(*this, duration_ns);
	}

	void ProfilerManager::outputSection(std::ostream& os, const std::chrono::nanoseconds duration_ns, std::string section)
	{
		const auto duration_us = std::chrono::duration_cast<std::chrono::microseconds>(duration_ns);
		const auto duration_ms = std::chrono::duration_cast<std::chrono::milliseconds>(duration_ns);
		const auto duration_s = std::chrono::duration_cast<std::chrono::seconds>(duration_ns);

		os << "     | " << section << ": ";

		// NOTE: Branching here isn't that slow
		// We're outputting to cout anyway...
		if (duration_ns.count() < 1000)
		{
			os << std::setprecision(3) << duration_ns.count() << "ns";
		}
		else if (duration_us.count() < 1000)
		{
			auto us = static_cast<double>(duration_us.count());
			us += (static_cast<double>(duration_ns.count()) - 1000) / 1000;

			os << std::setprecision(3) << us << "us";
		}
		else if (duration_ms.count() < 1000)
		{
			auto ms = static_cast<double>(duration_ms.count());
			ms += (static_cast<double>(duration_us.count()) - 1000) / 1000;

			os << std::setprecision(3) << ms << "ms";
		}
		else
		{
			auto s = static_cast<double>(duration_s.count());
			s += (static_cast<double>(duration_ms.count()) - 1000) / 1000;

			// TODO: Make this work for 1000's of seconds
			os << std::setprecision(3) << s << "s";
		}

		os << std::endl;
	}


	std::ostream& operator<<(std::ostream& os, const ProfilerManager& pm)
	{
		os << "Profiler:" << std::endl;
		for (auto domain : pm.getCompletedProfiles())
		{
			os << "=======" << std::endl;
			os << "Domain: " << domain.first << std::endl;
			os << "=======" << std::endl;

			// TODO: Consider adding a flag to the profiler manager that does this?
			// Then need to flip use of duration/section name
			// auto sortedDomain = Utils::Collections::flip_map(domain.second);

			std::chrono::nanoseconds total_ns{ };

			for (auto section : domain.second)
			{
				const auto duration_ns = section.second;
				total_ns += duration_ns;

				const auto title = section.first;

				ProfilerManager::outputSection(os, duration_ns, title);
			}

			os << "     | " << std::endl;
			ProfilerManager::outputSection(os, total_ns, "Total");
		}
		return os;
	}
}

#endif