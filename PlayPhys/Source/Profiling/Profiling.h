#pragma once

#include "Profiler.h"

#if PP_PROFILER_ENABLED

// Macro resolution/combiners
// https://stackoverflow.com/a/17624752/1351478
#define PP_CAT(a, b) PP_CAT_I(a, b)
#define PP_CAT_I(a, b) PP_CAT_II(~, a ## b)
#define PP_CAT_II(p, res) res

#define PROFILE_NAME(base) PP_CAT(base, __COUNTER__)

// Using token concatenation for keep_alive name wrangling, this allows multiple profilers in the same scope
// Uses arguments as string literals, meaning that they can use existing names without issue
// https://en.wikipedia.org/wiki/C_preprocessor#Token_concatenation

#define PROFILE(section_name, specific_name) \
	auto PROFILE_NAME(keep_alive_##section_name##specific_name) \
	= PP::Profiler(#section_name, #specific_name);

#else

// set timing macros to nothing
#define PROFILE(section_name, specific_name)

#endif
