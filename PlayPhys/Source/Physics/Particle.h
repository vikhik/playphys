#pragma once
#include "../Math/Math.h"

namespace PP::Physics
{
	using namespace Math;

	class Particle
	{
	public:
		explicit Particle(Vec3f position = { 0, 0, 0 }, Vec3f velocity = { 0, 0, 0 }, float mass = 1.0) noexcept
			: position(position), velocity(velocity), mass(mass),
			inv_mass(1 / mass), current_force({ 0, 0, 0 })
		{
		}

		~Particle() = default;

		float getForce() const noexcept;
		float getMomentum() const;

		friend std::ostream& operator<<(std::ostream& os, const Particle& particle);
		void prettyPrint(std::ostream& os) const;

	private:
		Vec3f position; // 12 bytes
		Vec3f velocity; // 12 bytes
		float mass; // 4 bytes
		float inv_mass; // 4 bytes?

		Vec3f current_force;
	};
}