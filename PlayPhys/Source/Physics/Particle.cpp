#include "enginepch.h"

#include "Particle.h"

namespace PP::Physics
{
	float Particle::getForce() const noexcept
	{
		return this->mass;
	}

	float Particle::getMomentum() const
	{
		return this->velocity.magnitude() * this->mass;
	}

	void Particle::prettyPrint(std::ostream& os) const
	{
		os << "Particle: " << std::endl
			<< "  Position: " << position << std::endl
			<< "  Velocity: " << velocity << std::endl
			<< "      Mass: " << mass << std::endl;
	}

	std::ostream& operator<<(std::ostream& os, const Particle& particle)
	{
		os << "Particle: "
			<< particle.position << " "
			<< particle.velocity << " "
			<< particle.mass;

		return os;
	}
}