#include "enginepch.h"

#include "Game.h"
#include "Platform/Platform.h"
#include <cassert>

namespace PP
{
	void Game::init(const std::vector<std::string_view> args)
	{
		PROFILE(Game, init);

		Utils::Filesystem::resetCwd(args[0]);

		platform_system = Platform::getPlatformSystem();
		platform_system->init();

		graphics_system = Graphics::getGraphicsSystem();
		graphics_system->init(platform_system->getWindow());

		Ensures(platform_system);
		Ensures(graphics_system);
	}

	void Game::run()
	{
		PROFILE(Game, run);

		Expects(platform_system);
		Expects(graphics_system);

		platform_system->run();
		graphics_system->run();
	}

	void Game::cleanup()
	{
		PROFILE(Game, cleanup);

		Expects(platform_system);
		Expects(graphics_system);

		graphics_system->cleanup();
		graphics_system.release();

		platform_system->cleanup();
		platform_system.release();

		Ensures(!platform_system);
		Ensures(!graphics_system);
	}

	bool Game::getShouldClose()
	{
		return force_close || (platform_system && platform_system->getShouldClose());
	}

	void Game::forceClose()
	{
		force_close = true;
	}
}
