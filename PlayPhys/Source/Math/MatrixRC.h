﻿// PlayPhys MatrixRC.h
// Last Edited: 20/10/2016
// First Created: 20/10/2016
// 
// Author: Vikram Saran
// Much help from http://www.reedbeta.com/blog/on-vector-math-libraries/

#pragma once
#include "Utils/Types.h"

namespace PP::Math
{
	/*
	* TODO: Constructors
	* TODO: Component-wise operators (+* etc.)
	* TODO: Free operators (dot(a,b))
	* TODO: min, max, abs, clamp, saturate, and lerp
	* TODO: (matrix only) MinComponent, MaxComponent
	* TODO: ensure Cross-compatibility, give raw access to a vector or matrix as a pointer or C array, with an implicit conversion.
	* TODO: Add Affine and Point type specialisations
	* TODO: Add separate SIMD Vector/Matrix library
	*/

	template <typename TValue, impl::size_type rows, impl::size_type cols>
	struct MatrixRC
	{
		gsl::span<TValue, rows*cols> data{};
	};

	// Specializations for 3x3 and 4x4
	using Mat33f = MatrixRC<real32, 3, 3>;
	using Mat44f = MatrixRC<real32, 4, 4>;
}