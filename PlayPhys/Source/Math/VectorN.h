// PlayPhys VectorN.h
// Last Edited: 20/10/2016
// First Created: 20/10/2016
// 
// Author: Vikram Saran
// Much help from http://www.reedbeta.com/blog/on-vector-math-libraries/

#include <gsl/gsl>
#include <iostream>
#include "Utils/Concepts.h"
#include "Utils/Types.h"
#include "Utils/Defines.h"

namespace PP::Math
{
	/*
	 * DONE: Constructors
	 * DONE: Component-wise operators (+* etc.)
		* DONE: Add +=, *=, etc.
	 * DONE: Free operators (dot(a,b))
		* DONE: free abs, clamp, lerp
		* TODO: add VectorN<TValue, 3> Cross
	 * DONE: ensure Cross-compatibility, give raw access to a vector or matrix as a pointer or C array, with an implicit conversion.
	 * DONE: Add constexpr to constructors where possible
	 * DONE: Add constructors for common subparts (i.e. new Vec4f(vec3, w);)
	 * DONE: Add constructors from TValue* or TValue[]
		* NOTE: Look at methods to improve by using bounds checking?
		* NOTE: Look at constructor from TValue[]?
	 * DONE: min, max, abs, clamp, and lerp
		* NOTE: Consider adding saturate?
	 * TODO: Use Concepts and Constraints to only allow instantiation with correct types
	 * TODO: Abstract interpolation function which takes a fn? (or prebuild several?)
	 * TODO: Add Affine and Point type specialisations
	 * TODO: Add separate SIMD Vector/Matrix library
	 */

	namespace impl
	{
		// Using CTRP (https://en.wikipedia.org/wiki/Curiously_recurring_template_pattern)
		// Allows us to generate base functions that apply to all specialisations of VectorN

		using size_type = uint32;
		using namespace PP::Concepts;

		template <Numeric TValue, size_type n, typename TDerived>
		struct VectorBase
		{
			/* Statics and typedefs*/

			using value_type = TValue;
			using size_type = size_type;
			static const size_type count = n;

			/* Functions */

			// Returns the square of the length/magnitude of the vector
			TValue lengthSquared() const
			{
				TValue result{ 0 };

				for (size_type i = 0; i < n; ++i)
				{
					result += (*this)[i] * (*this)[i];
				}

				return result;
			}

			// Returns the square of the length/magnitude of the vector
			TValue magnitudeSquared() const { return lengthSquared(); }

			// Returns the magnitude/length of the vector
			TValue length() const
			{
				// TODO: Check how this interacts with integer Vectors
				return sqrt(lengthSquared());
			}

			// Returns the magnitude/length of the vector
			TValue magnitude() const { return length(); }

			// Returns the minimal TValue in the vector
			TValue min() const
			{
				TValue result = (*this)[0];

				for (size_type i = 1; i < n; ++i)
				{
					if ((*this)[i] < result)
					{
						result = (*this)[i];
					}
				}

				return result;
			}

			// Returns the maximal TValue in the vector
			TValue max() const
			{
				TValue result = (*this)[0];

				for (size_type i = 1; i < n; ++i)
				{
					if ((*this)[i] > result)
					{
						result = (*this)[i];
					}
				}

				return result;
			}

			// SIDE-EFFECTS: (in-place) sets vector values to be positive
			// RETURNS: The vector
			TDerived& abs()
			{
				for (size_type i = 0; i < n; ++i)
				{
					(*this)[i] = (*this)[i] > 0 ? (*this)[i] : -(*this)[i];
				}

				return static_cast<TDerived&>(*this);
			}

			// SIDE-EFFECTS: (in-place) clamps values in the vector
			// RETURNS: The vector
			TDerived& clamp(TValue min, TValue max)
			{
				for (size_type i = 0; i < n; ++i)
				{
					(*this)[i] = (*this)[i] < min ? min : (*this)[i] < max ? (*this)[i] : max;
				}

				return static_cast<TDerived&>(*this);
			}

			/* Access */

			// const by index
			const TValue& operator[](const size_type i) const
			{
				Expects(i >= 0 && i < n);

				auto& derived = static_cast<const TDerived&>(*this);
				return gsl::at(derived.data, i);
			}

			// non-const by index
			TValue& operator[](const size_type i)
			{
				Expects(i >= 0 && i < n);

				auto& derived = static_cast<TDerived&>(*this);
				return gsl::at(derived.data, i);
			}

			// const dereference
			const TValue* operator*() const noexcept
			{
				return static_cast<const TValue*>(static_cast<const TDerived&>(*this).data);
			}

			// non-const dereference
			TValue* operator*() noexcept
			{
				return static_cast<TValue*>(static_cast<TDerived&>(*this).data);
			}

			/* Equality */

			friend bool operator==(const TDerived& vec, const TDerived& otherVec)
			{
				for (size_type i = 0; i < vec.count; ++i)
				{
					if (vec[i] != otherVec[i])
					{
						return false;
					}
				}
				return true;
			}

			/* Unary Operations */

			// Negation
			TDerived operator-() const
			{
				TDerived result{ };

				for (size_type i = 0; i < n; ++i)
				{
					result[i] = -(*this)[i];
				}

				return result;
			}

			/* Binary Infix Operators */

			/* Scalar Operations */

			TDerived& operator+=(const TValue scalar)
			{
				for (size_type i = 0; i < n; ++i)
				{
					(*this)[i] = (*this)[i] + scalar;
				}

				return static_cast<TDerived&>(*this);
			}

			TDerived& operator-=(const TValue scalar)
			{
				*this += -scalar;

				return static_cast<TDerived&>(*this);
			}

			TDerived& operator*=(const TValue scalar)
			{
				for (size_type i = 0; i < n; ++i)
				{
					(*this)[i] = (*this)[i] * scalar;
				}

				return static_cast<TDerived&>(*this);
			}

			TDerived& operator/=(const TValue scalar)
			{
				Expects(scalar != 0);
				*this *= 1 / scalar;

				return static_cast<TDerived&>(*this);
			}

			/* Vector Operations */

			TDerived& operator+=(const TDerived& otherVec)
			{
				for (size_type i = 0; i < n; ++i)
				{
					(*this)[i] = (*this)[i] + otherVec[i];
				}

				return static_cast<TDerived&>(*this);
			}

			TDerived& operator-=(const TDerived& otherVec)
			{
				(*this) += -otherVec;
				return static_cast<TDerived&>(*this);
			}

			TDerived& operator*=(const TDerived& otherVec)
			{
				for (size_type i = 0; i < n; ++i)
				{
					(*this)[i] = (*this)[i] * otherVec[i];
				}

				return static_cast<TDerived&>(*this);
			}

			TDerived& operator/=(const TDerived& otherVec)
			{
				// TODO: add a 1/x function!
				//(*this) *= 1 / otherVec;

				for (size_type i = 0; i < n; ++i)
				{
					Expects(otherVec[i] != 0);
					(*this)[i] = (*this)[i] / otherVec[i];
				}

				return static_cast<TDerived&>(*this);
			}

			/* Binary Infix Operators */

			/* Scalar Operations */

			TDerived operator+(const TValue scalar) const
			{
				TDerived result{ };

				for (size_type i = 0; i < n; ++i)
				{
					result[i] = (*this)[i] + scalar;
				}

				return result;
			}

			TDerived operator-(const TValue scalar) const
			{
				return (*this) + (-scalar);
			}

			TDerived operator*(const TValue scalar) const
			{
				TDerived result{ };

				for (size_type i = 0; i < n; ++i)
				{
					result[i] = (*this)[i] * scalar;
				}

				return result;
			}

			TDerived operator/(const TValue scalar) const
			{
				Expects(scalar != 0);
				return (*this) * (1 / scalar);
			}

			/* Vector Operations */

			TDerived operator+(const TDerived& otherVec) const
			{
				TDerived result = static_cast<const TDerived&>(*this);

				result += otherVec;

				return result;
			}

			TDerived operator-(const TDerived& otherVec) const
			{
				TDerived result = static_cast<const TDerived&>(*this);

				result -= otherVec;

				return result;
			}

			TDerived operator*(const TDerived& otherVec) const
			{
				TDerived result = static_cast<const TDerived&>(*this);

				result *= otherVec;

				return result;
			}

			TDerived operator/(const TDerived& otherVec) const
			{
				TDerived result = static_cast<const TDerived&>(*this);

				result /= otherVec;

				return result;
			}

			/* Static Operations */

			// Static dot product
			static TValue dot(const TDerived& lhs, const TDerived& rhs)
			{
				return lhs * rhs;
			}

			static const TDerived Zero;
		};

		template <Numeric TValue, size_type n, typename TDerived>
		const TDerived VectorBase<TValue, n, TDerived>::Zero{};
	}

	/*********************************************/
	/* First level specialization for VectorBase */
	/*********************************************/

	template <typename TValue, impl::size_type n>
	struct VectorN : impl::VectorBase<TValue, n, VectorN<TValue, n>>
	{
		TValue data[n];
	};

	/******************/
	/* Free Operators */
	/******************/

	// The format is (x, y, z, ..n)
	template <typename TValue, impl::size_type n>
	std::ostream& operator<<(std::ostream& os, const VectorN<TValue, n>& vec)
	{
		os << "(";
		for (impl::size_type i = 0; i < n; ++i)
		{
			if (i != 0)
			{
				os << ", ";
			}
			os << vec[i];
		}
		os << ")";
		return os;
	}

	// The format is (x, y, z, ..n)
	template <typename TValue, impl::size_type n>
	std::istream& operator >>(std::istream& is, VectorN<TValue, n>& vec)
	{
		static_assert(std::is_integral_v<TValue> || std::is_floating_point_v<TValue>, "Not implemented");

		char c;
		is.read(&c, 1);
		if (c != '(')
		{
			is.setstate(std::ios::failbit);
			return is;
		}

		impl::size_type i = 0;

		for (std::string chunk; std::getline(is, chunk, ',');)
		{
			if (i == n - 1)
			{
				if (chunk.back() != ')')
				{
					is.setstate(std::ios::failbit);
					return is;
				}
				chunk.pop_back();
			}

			if constexpr (std::is_integral_v<TValue>)
			{
				vec[i] = std::stoi(chunk);
			}
			else if constexpr (std::is_floating_point_v<TValue>)
			{
				vec[i] = std::stof(chunk);
			}
			++i;
		}
		return is;
	}

	// TODO: Investigate a way to make these binary operators external methods
	//template <typename TValue, size_type n, typename TDerived>
	//inline TDerived operator*(TDerived lhs, const TDerived& rhs)
	//{
	//	return lhs *= rhs;
	//}

	/******************/
	/* Free Functions */
	/******************/

	template <typename T>
	typename T::value_type dot(const T& lhs, const T& rhs)
	{
		static_assert(
			std::is_base_of_v<
			/* Base */ impl::VectorBase<typename T::value_type, T::count, T>,
			/* Derived */ T>,
			"T is not a suitable VectorBase");

		typename T::value_type result{ };

		for (impl::size_type i = 0; i < T::count; ++i)
		{
			result += lhs[i] * rhs[i];
		}

		return result;
	}

	template <typename T>
	T lerp(const T& lhs, const T& rhs, const real32 alpha)
	{
		assert(alpha >= 0.f && alpha <= 1.f);

		static_assert(
			std::is_base_of_v<
			/* Base */ impl::VectorBase<typename T::value_type, T::count, T>,
			/* Derived */ T>,
			"T is not a suitable VectorBase");

		T result{ };

		for (impl::size_type i = 0; i < T::count; ++i)
		{
			result[i] = (lhs[i] * (1 - alpha)) + (rhs[i] * alpha);
		}

		return result;
	}

	template <typename T>
	T abs(const T& vec)
	{
		static_assert(
			std::is_base_of_v<
			/* Base */ impl::VectorBase<typename T::value_type, T::count, T>,
			/* Derived */ T>,
			"T is not a suitable VectorBase");

		T result{ vec };

		return result.abs();
	}

	template <typename T>
	T clamp(const T& vec, const typename T::value_type min, const typename T::value_type max)
	{
		static_assert(
			std::is_base_of_v<
			/* Base */ impl::VectorBase<typename T::value_type, T::count, T>,
			/* Derived */ T>,
			"T is not a suitable VectorBase");

		T result{ vec };

		return result.clamp(min, max);
	}

	/************************************************/
	/* Second level specializations for n = 2, 3, 4 */
	/************************************************/

	// n = 2

	template <typename TValue>
	struct VectorN<TValue, 2> : impl::VectorBase<TValue, 2, VectorN<TValue, 2>>
	{
		union
		{
			TValue data[2];

			struct
			{
				TValue x, y;
			};
		};

		constexpr VectorN() noexcept : data{ 0, 0 } { }
		constexpr explicit VectorN(TValue fill) noexcept : data{ fill, fill } { }
		constexpr VectorN(TValue ax, TValue ay) noexcept : data{ ax, ay } { }

		VectorN(nullptr_t) = delete;
		constexpr explicit VectorN(const TValue* arr) noexcept : data{ arr[0], arr[1] } { }
		constexpr explicit VectorN(const gsl::span<TValue, 2> arr) noexcept : data{ arr[0], arr[1] } {}
	};

	using Vec2i = VectorN<int32, 2>;
	using Vec2f = VectorN<real32, 2>;
	using Vec2d = VectorN<real64, 2>;

	// n = 3

	template <typename TValue>
	struct VectorN<TValue, 3> : impl::VectorBase<TValue, 3, VectorN<TValue, 3>>
	{
		union
		{
			TValue data[3];

			struct
			{
				TValue x, y, z;
			};

			struct
			{
				TValue r, g, b;
			};

			VectorN<TValue, 2> xy;
		};

		constexpr VectorN() noexcept : data{ 0, 0, 0 } { }
		constexpr explicit VectorN(TValue fill) noexcept : data{ fill, fill, fill } { }
		constexpr VectorN(TValue ax, TValue ay, TValue az) noexcept : data{ ax, ay, az } { }

		constexpr explicit VectorN(const VectorN<TValue, 2>& axy) noexcept : data{ axy.x, axy.y, 0 } { }
		constexpr VectorN(const VectorN<TValue, 2>& axy, TValue az) noexcept : data{ axy.x, axy.y, az } { }

		VectorN(nullptr_t) = delete;
		constexpr explicit VectorN(const TValue* arr) noexcept : data{ arr[0], arr[1], arr[2] } { }
		constexpr explicit VectorN(const gsl::span<TValue, 3> arr) noexcept : data{ arr[0], arr[1], arr[2] } {}
	};

	using Vec3i = VectorN<int32, 3>;
	using Vec3f = VectorN<real32, 3>;
	using Vec3d = VectorN<real64, 3>;

	// n = 4

	template <typename TValue>
	struct VectorN<TValue, 4> : impl::VectorBase<TValue, 4, VectorN<TValue, 4>>
	{
		union
		{
			TValue data[4];

			struct
			{
				TValue x, y, z, w;
			};

			struct
			{
				TValue r, g, b, a;
			};

			VectorN<TValue, 3> xyz;
			VectorN<TValue, 3> rgb;
			VectorN<TValue, 2> xy;
		};

		constexpr VectorN() noexcept : data{ 0, 0, 0, 0 } { }
		constexpr explicit VectorN(TValue fill) noexcept : data{ fill, fill, fill, fill } { }
		constexpr VectorN(TValue ax, TValue ay, TValue az, TValue aw) noexcept : data{ ax, ay, az, aw } { }

		constexpr explicit VectorN(const VectorN<TValue, 2>& axy) noexcept : data{ axy.x, axy.y, 0, 0 } { }
		constexpr VectorN(const VectorN<TValue, 2>& axy, TValue az) noexcept : data{ axy.x, axy.y, az, 0 } { }
		constexpr VectorN(const VectorN<TValue, 2>& axy, TValue az, TValue aw) noexcept : data{ axy.x, axy.y, az, aw } { }
		constexpr VectorN(const VectorN<TValue, 2>& axy, const VectorN<TValue, 2>& azw) noexcept : data{ axy.x, axy.y, azw.x, azw.y } { }

		constexpr explicit VectorN(const VectorN<TValue, 3>& axyz) noexcept : data{ axyz.x, axyz.y, axyz.z, 0 } { }
		constexpr VectorN(const VectorN<TValue, 3>& axyz, TValue aw) noexcept : data{ axyz.x, axyz.y, axyz.z, aw } { }

		VectorN(nullptr_t) = delete;
		constexpr explicit VectorN(const TValue* arr) noexcept : data{ arr[0], arr[1], arr[2], arr[3] } { }
		constexpr explicit VectorN(const gsl::span<TValue, 4> arr) noexcept : data{ arr[0], arr[1], arr[2], arr[3] } {}
	};

	// TODO: typedef Vec4f/Vec4d Quaternion;
	// perhaps with a macro flag for precision?
	using Vec4i = VectorN<int32, 4>;
	using Vec4f = VectorN<real32, 4>;
	using Vec4d = VectorN<real64, 4>;
}