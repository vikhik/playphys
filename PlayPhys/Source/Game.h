#pragma once

#include <vector>
#include <string>
#include "Platform/PlatformSystem.h"
#include "Graphics/GraphicsSystem.h"
#include "Utils/Defines.h"

namespace PP
{
	class Game
	{
	public:
		PP_API Game() noexcept = default;

		Game(const Game&) = delete;
		Game(Game&&) = delete;
		Game& operator=(const Game&) = delete;
		Game& operator=(Game&&) = delete;

		PP_API ~Game() = default;

		PP_API virtual void init(const std::vector<std::string_view> args);
		PP_API virtual void run();
		PP_API void cleanup();

		PP_API virtual bool getShouldClose();
		PP_API void forceClose();

	private:
		bool force_close = false;
		std::unique_ptr<Platform::PlatformSystem> platform_system;
		std::unique_ptr<Graphics::GraphicsSystem> graphics_system;
	};
}
