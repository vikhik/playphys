#pragma once

#include "GraphicsSystem.h"
#include <memory>

#if PP_GRAPHICS_SYSTEM == VULKAN

#include "Vulkan/VulkanSystem.h"

#else

PP_NOT_IMPLEMENTED;

#endif

namespace PP::Graphics
{
	std::unique_ptr<GraphicsSystem> getGraphicsSystem();

#if PP_GRAPHICS_SYSTEM == VULKAN

	inline std::unique_ptr<GraphicsSystem> getGraphicsSystem()
	{
		return std::make_unique<VulkanSystem>();
	}

#endif
}