#pragma once

#include "Utils/Types.h"
#include "Platform/PlatformSystem.h"

namespace PP::Graphics
{
	class GraphicsSystem
	{
	public:
		GraphicsSystem() noexcept = default;
		GraphicsSystem(const GraphicsSystem&) = delete;
		GraphicsSystem(GraphicsSystem&&) = delete;
		GraphicsSystem& operator=(const GraphicsSystem&) = delete;
		GraphicsSystem& operator=(GraphicsSystem&&) = delete;
		virtual ~GraphicsSystem() = default;

		virtual void init(Platform::IWindow& window) = 0;
		virtual void run() = 0;
		virtual void cleanup() = 0;

		//protected:
		//	virtual void mainLoop() = 0;
	};
}