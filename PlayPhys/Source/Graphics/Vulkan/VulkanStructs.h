#pragma once

#include <vulkan/vulkan.h>
#include <vector>
#include <stack>
#include <string>

namespace PP::Graphics
{
	struct SwapChainSupportDetails
	{
		// Doesn't auto-initialise because Extents don't auto-initialise
		VkSurfaceCapabilitiesKHR capabilities{ };
		std::vector<VkSurfaceFormatKHR> formats{ };
		std::vector<VkPresentModeKHR> presentModes{ };
	};

	struct QueueFamilyIndices
	{
		int graphicsFamily = -1;
		int presentFamily = -1;

		bool isComplete() const noexcept
		{
			return graphicsFamily >= 0 && presentFamily >= 0;
		}
	};

	struct requirementsSupportedResult
	{
		bool allSupported;
		std::stack<std::string> missingRequirements;
	};
}