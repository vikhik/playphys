#pragma once

#include <vector>
#include "../GraphicsSystem.h"
#include "VulkanStructs.h"

namespace PP::Graphics
{
	class VulkanSystem : public GraphicsSystem
	{
	public:
		void init(Platform::IWindow& window) override;
		void run() override;
		void cleanup() override;

	private:
		/* Core */
		void drawFrame();

		/* Initilisation */
		void createInstance();
		void setupDebugCallback();
		void pickPhysicalDevice();
		void createLogicalDevice();
		void createSurface(Platform::IWindow& window);
		void createSwapChain();
		void createImageViews();
		void createRenderPass();
		void createGraphicsPipeline();
		void createFramebuffers();
		void createCommandPool();
		void createCommandBuffers();
		void createSemaphores();

		/* Utility */
		int rateDeviceSuitability(const VkPhysicalDevice& device) const;
		bool isDeviceSuitable(const VkPhysicalDevice& device) const;
		QueueFamilyIndices findQueueFamilies(const VkPhysicalDevice& device) const;
		SwapChainSupportDetails querySwapChainSupport(const VkPhysicalDevice& device) const;
		bool swapChainAdequate(const VkPhysicalDevice& device) const;
		VkExtent2D chooseSwapExtent(const VkSurfaceCapabilitiesKHR& capabilities) const noexcept;
		VkShaderModule createShaderModule(const std::vector<char>& code);

		/* Static */
		static requirementsSupportedResult checkValidationLayerSupport();
		static std::vector<const char*> getRequiredInstanceExtensions();
		static bool checkDeviceExtensionSupport(const VkPhysicalDevice& device);
		static VkSurfaceFormatKHR chooseSwapSurfaceFormat(const std::vector<VkSurfaceFormatKHR>& availableFormats) noexcept;
		static VkPresentModeKHR chooseSwapPresentMode(const std::vector<VkPresentModeKHR> availablePresentModes) noexcept;

		/* Callbacks */
		static VKAPI_ATTR VkBool32 VKAPI_CALL debugCallback(
			VkDebugReportFlagsEXT flags,
			VkDebugReportObjectTypeEXT objType,
			uint64_t obj,
			size_t location,
			int32_t code,
			const char* layerPrefix,
			const char* msg,
			void* userData);

		/* Properties */

		// Instance
		VkInstance instance{ };

		// Debug Callback
		VkDebugReportCallbackEXT callback{ };

		// Physical Device
		VkPhysicalDevice physicalDevice{ };

		// Surface
		VkSurfaceKHR surface{ };

		// Logical Device
		VkDevice logicalDevice{ };
		VkQueue graphicsQueue{ };
		VkQueue presentQueue{ };

		// Swap Chain
		VkSwapchainKHR swapChain{ };
		std::vector<VkImage> swapChainImages{ };
		VkFormat swapChainImageFormat{ };
		VkExtent2D swapChainExtent{ };

		// Image Views
		std::vector<VkImageView> swapChainImageViews{ };

		// Render Pass
		VkRenderPass renderPass{ };

		// Pipeline
		// Layout sets up 'uniform' values (transform matrix, etc.) for shaders
		VkPipelineLayout pipelineLayout{ };
		// 
		VkPipeline graphicsPipeline{ };

		// Frame buffers
		std::vector<VkFramebuffer> swapChainFramebuffers{ };

		// Commands
		VkCommandPool commandPool{ };
		std::vector<VkCommandBuffer> commandBuffers{ };

		// Semaphores
		// Associated with resources and can be used to marshal ownership of shared data. 
		// Their status is not visible to the host.
		// http://www.openvulkan.com/2016/03/24/synchronization-for-vulkan-fences-semaphores-events-barriers/
		VkSemaphore imageAvailableSemaphore{ };
		VkSemaphore renderFinishedSemaphore{ };
	};
}