#include "enginepch.h"

#include "VulkanSystem.h"
#include "VulkanCallbacks.h"
#include "VulkanConstants.h"
#include "Utils/Utils.h"
#include "Profiling/Profiling.h"
#include "Platform/PlatformSystem.h"

#if PP_PLATFORM_SYSTEM == PP_PLATFORM_GLFW
#include "Platform/GLFW/GLFWSystem.h"
#include <GLFW/glfw3.h>
#endif

namespace PP::Graphics
{
	void VulkanSystem::init(Platform::IWindow& window)
	{
		PROFILE(GraphicsSystem, init);

		createInstance();
		setupDebugCallback();
		createSurface(window);

		pickPhysicalDevice();
		createLogicalDevice();

		createSwapChain();
		createImageViews();

		createRenderPass();
		createGraphicsPipeline();

		createFramebuffers();
		createCommandPool();
		createCommandBuffers();

		createSemaphores();
	}

	void VulkanSystem::run()
	{
		PROFILE(GraphicsSystem, run);

		{
			PROFILE(GraphicsSystem, drawFrame);

			// 1. Acquire an image from the swap chain
			uint32_t imageIndex;
			vkAcquireNextImageKHR(logicalDevice, swapChain, std::numeric_limits<uint64_t>::max(), imageAvailableSemaphore, VK_NULL_HANDLE, &imageIndex);

			// 2. Execute the command buffer with that image as attachment in the framebuffer
			VkSubmitInfo submitInfo{ };
			submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;

			const VkSemaphore waitSemaphores[] = { imageAvailableSemaphore };
			const VkPipelineStageFlags waitStages[] = { VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT };
			submitInfo.waitSemaphoreCount = 1;
			submitInfo.pWaitSemaphores = &waitSemaphores[0];
			submitInfo.pWaitDstStageMask = &waitStages[0];

			// Prepare to submit some commands (pCommandBuffers)
			submitInfo.commandBufferCount = 1;
			submitInfo.pCommandBuffers = &gsl::at(commandBuffers, imageIndex);

			const VkSemaphore signalSemaphores[] = { renderFinishedSemaphore };
			submitInfo.signalSemaphoreCount = 1;
			submitInfo.pSignalSemaphores = &signalSemaphores[0];

			if (vkQueueSubmit(graphicsQueue, 1, &submitInfo, VK_NULL_HANDLE) != VK_SUCCESS)
			{
				throw std::runtime_error("failed to submit draw command buffer!");
			}

			// 3. Return the image to the swap chain for presentation
			VkPresentInfoKHR presentInfo{ };
			presentInfo.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;

			presentInfo.waitSemaphoreCount = 1;
			presentInfo.pWaitSemaphores = &signalSemaphores[0];

			const VkSwapchainKHR swapChains[] = { swapChain };
			presentInfo.swapchainCount = 1;
			presentInfo.pSwapchains = &swapChains[0];
			presentInfo.pImageIndices = &imageIndex;

			presentInfo.pResults = nullptr; // Optional

			vkQueuePresentKHR(presentQueue, &presentInfo);
		}

		{
			PROFILE(GraphicsSystem, waitIdle);

			// explicitly waiting for presentation to finish before starting to draw the next frame
			vkQueueWaitIdle(presentQueue);
		}
	}

	void VulkanSystem::cleanup()
	{
		PROFILE(GraphicsSystem, cleanup);

		vkDeviceWaitIdle(logicalDevice);

		vkDestroySemaphore(logicalDevice, renderFinishedSemaphore, nullptr);
		vkDestroySemaphore(logicalDevice, imageAvailableSemaphore, nullptr);

		vkDestroyCommandPool(logicalDevice, commandPool, nullptr);

		for (size_t i = 0; i < swapChainFramebuffers.size(); i++)
		{
			vkDestroyFramebuffer(logicalDevice, gsl::at(swapChainFramebuffers, i), nullptr);
		}

		vkDestroyPipeline(logicalDevice, graphicsPipeline, nullptr);
		vkDestroyPipelineLayout(logicalDevice, pipelineLayout, nullptr);
		vkDestroyRenderPass(logicalDevice, renderPass, nullptr);

		for (size_t i = 0; i < swapChainImageViews.size(); i++)
		{
			vkDestroyImageView(logicalDevice, gsl::at(swapChainImageViews, i), nullptr);
		}

		vkDestroySwapchainKHR(logicalDevice, swapChain, nullptr);
		vkDestroyDevice(logicalDevice, nullptr);

		DestroyDebugReportCallbackEXT(instance, callback, nullptr);
		vkDestroySurfaceKHR(instance, surface, nullptr);
		vkDestroyInstance(instance, nullptr);
	}

	/* Initialisation */
	void VulkanSystem::createInstance()
	{
		if (enableValidationLayers)
		{
			auto validationSupported = checkValidationLayerSupport();

			if (!validationSupported.allSupported)
			{
				while (!validationSupported.missingRequirements.empty())
				{
					std::cerr << "Missing validation layer: " << validationSupported.missingRequirements.top().c_str() << std::endl;
					validationSupported.missingRequirements.pop();
				}

				throw std::runtime_error("Validation layers requested, but not available!");
			}
		}

		VkApplicationInfo appInfo{ };
		appInfo.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
		appInfo.pApplicationName = "PlayPhys";
		appInfo.applicationVersion = VK_MAKE_VERSION(0, 0, 0);
		appInfo.pEngineName = "PlayPhys";
		appInfo.engineVersion = VK_MAKE_VERSION(0, 0, 0);
		appInfo.apiVersion = VK_API_VERSION_1_0;

		VkInstanceCreateInfo instanceCreateInfo{ };
		instanceCreateInfo.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
		instanceCreateInfo.pApplicationInfo = &appInfo;

		auto instanceExtensions = getRequiredInstanceExtensions();
		instanceCreateInfo.enabledExtensionCount = gsl::narrow<uint32>(instanceExtensions.size());
		instanceCreateInfo.ppEnabledExtensionNames = instanceExtensions.data();

		if (enableValidationLayers)
		{
			instanceCreateInfo.enabledLayerCount = gsl::narrow<uint32>(validationLayers.size());
			instanceCreateInfo.ppEnabledLayerNames = validationLayers.data();
		}
		else
		{
			instanceCreateInfo.enabledLayerCount = 0;
		}

		if (vkCreateInstance(&instanceCreateInfo, nullptr, &instance) != VK_SUCCESS)
		{
			throw std::runtime_error("Failed to create VK instance!");
		}
	}

	void VulkanSystem::setupDebugCallback()
	{
		if (!enableValidationLayers) return;

		// More settings can be done using vk_layer_settings.txt in the application directory
		VkDebugReportCallbackCreateInfoEXT callbackCreateInfo{ };
		callbackCreateInfo.sType = VK_STRUCTURE_TYPE_DEBUG_REPORT_CALLBACK_CREATE_INFO_EXT;
		callbackCreateInfo.flags = VK_DEBUG_REPORT_ERROR_BIT_EXT | VK_DEBUG_REPORT_WARNING_BIT_EXT;
		callbackCreateInfo.pfnCallback = debugCallback;

		if (CreateDebugReportCallbackEXT(instance, &callbackCreateInfo, nullptr, &callback) != VK_SUCCESS)
		{
			throw std::runtime_error("Failed to set up debug callback!");
		}
	}

	void VulkanSystem::pickPhysicalDevice()
	{
		uint32_t deviceCount;
		vkEnumeratePhysicalDevices(instance, &deviceCount, nullptr);

		if (deviceCount == 0)
		{
			throw std::runtime_error("failed to find GPUs with Vulkan support!");
		}

		std::vector<VkPhysicalDevice> devices(deviceCount);
		vkEnumeratePhysicalDevices(instance, &deviceCount, devices.data());

		constexpr auto useScore = true;
		if constexpr (useScore)
		{
			// Use an ordered map to automatically sort candidates by increasing score
			std::multimap<int, VkPhysicalDevice> candidates;

			for (const auto& device : devices)
			{
				auto score = rateDeviceSuitability(device);
				candidates.insert(std::make_pair(score, device));
			}

			// Check if the best candidate is suitable at all
			if (candidates.rbegin()->first > 0)
			{
				physicalDevice = candidates.rbegin()->second;
			}
			else
			{
				throw std::runtime_error("failed to find a suitable GPU!");
			}
		}
		else
		{
			for (const auto& device : devices)
			{
				if (isDeviceSuitable(device))
				{
					physicalDevice = device;
					break;
				}
			}

			if (physicalDevice == nullptr)
			{
				throw std::runtime_error("failed to find a suitable GPU!");
			}
		}
	}

	void VulkanSystem::createLogicalDevice()
	{
		const auto indices = findQueueFamilies(physicalDevice);

		std::vector<VkDeviceQueueCreateInfo> queueCreateInfos;
		std::set<uint32> uniqueQueueFamilyIndices = { gsl::narrow<uint32>(indices.graphicsFamily), gsl::narrow<uint32>(indices.presentFamily) };

		constexpr auto queuePriority = 1.0f;
		for (auto queueFamilyIndex : uniqueQueueFamilyIndices)
		{
			VkDeviceQueueCreateInfo queueCreateInfo{ };
			queueCreateInfo.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
			queueCreateInfo.queueFamilyIndex = { queueFamilyIndex };
			queueCreateInfo.queueCount = 1;
			queueCreateInfo.pQueuePriorities = &queuePriority;
			queueCreateInfos.push_back(queueCreateInfo);
		}

		VkPhysicalDeviceFeatures deviceFeatures{ };

		VkDeviceCreateInfo deviceCreateInfo{ };
		deviceCreateInfo.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
		deviceCreateInfo.queueCreateInfoCount = gsl::narrow<uint32>(queueCreateInfos.size());
		deviceCreateInfo.pQueueCreateInfos = queueCreateInfos.data();

		deviceCreateInfo.pEnabledFeatures = &deviceFeatures;

		deviceCreateInfo.enabledExtensionCount = gsl::narrow<uint32>(deviceExtensions.size());
		deviceCreateInfo.ppEnabledExtensionNames = deviceExtensions.data();

		if (enableValidationLayers)
		{
			deviceCreateInfo.enabledLayerCount = gsl::narrow<uint32>(validationLayers.size());
			deviceCreateInfo.ppEnabledLayerNames = validationLayers.data();
		}
		else
		{
			deviceCreateInfo.enabledLayerCount = 0;
		}

		if (vkCreateDevice(physicalDevice, &deviceCreateInfo, nullptr, &logicalDevice) != VK_SUCCESS)
		{
			throw std::runtime_error("Failed to create logical device!");
		}

		vkGetDeviceQueue(logicalDevice, indices.graphicsFamily, 0, &graphicsQueue);
		vkGetDeviceQueue(logicalDevice, indices.presentFamily, 0, &presentQueue);
	}

	void VulkanSystem::createSurface(Platform::IWindow& window)
	{
#if PP_PLATFORM_SYSTEM == PP_PLATFORM_GLFW
		if (glfwCreateWindowSurface(instance, static_cast<Platform::GLFWWindow&>(window).window, nullptr, &surface) != VK_SUCCESS)
		{
			throw std::runtime_error("failed to create window surface!");
		}
#else
		PP_NOT_IMPLEMENTED;
#endif
	}

	void VulkanSystem::createSwapChain()
	{
		auto swapChainSupport = querySwapChainSupport(physicalDevice);

		const auto surfaceFormat = chooseSwapSurfaceFormat(swapChainSupport.formats);
		const auto presentMode = chooseSwapPresentMode(swapChainSupport.presentModes);
		const auto extent = chooseSwapExtent(swapChainSupport.capabilities);

		// Now determine the queue length;
		auto imageCount = swapChainSupport.capabilities.minImageCount + 1;

		// Value of 0 here would mean no limit besides memory requirement
		if (swapChainSupport.capabilities.maxImageCount > 0 && imageCount > swapChainSupport.capabilities.maxImageCount)
		{
			imageCount = swapChainSupport.capabilities.maxImageCount;
		}

		VkSwapchainCreateInfoKHR swapChainCreateInfo{ };

		swapChainCreateInfo.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;
		swapChainCreateInfo.surface = surface;
		swapChainCreateInfo.minImageCount = imageCount;
		swapChainCreateInfo.imageFormat = surfaceFormat.format;
		swapChainCreateInfo.imageColorSpace = surfaceFormat.colorSpace;
		swapChainCreateInfo.imageExtent = extent;
		swapChainCreateInfo.imageArrayLayers = 1; // Used for stereoscopic 3D

		// Declare that we're rendering directly to the images in this swap chain
		// Could use VK_IMAGE_USAGE_TRANSFER_DST_BIT to do postprocessing or something else
		swapChainCreateInfo.imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;

		// Now we tell the swapchain how to interact with queue families
		const auto indices = findQueueFamilies(physicalDevice);
		const uint32_t queueFamilyIndices[] = { gsl::narrow<uint32_t>(indices.graphicsFamily), gsl::narrow<uint32_t>(indices.presentFamily) };

		if (indices.graphicsFamily != indices.presentFamily)
		{
			swapChainCreateInfo.imageSharingMode = VK_SHARING_MODE_CONCURRENT;
			swapChainCreateInfo.queueFamilyIndexCount = 2;
			swapChainCreateInfo.pQueueFamilyIndices = queueFamilyIndices;
		}
		else
		{
			swapChainCreateInfo.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE;
			swapChainCreateInfo.queueFamilyIndexCount = 0; // optional
			swapChainCreateInfo.pQueueFamilyIndices = nullptr; // optional
		}

		swapChainCreateInfo.preTransform = swapChainSupport.capabilities.currentTransform;
		swapChainCreateInfo.compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;

		swapChainCreateInfo.presentMode = presentMode;
		swapChainCreateInfo.clipped = VK_TRUE;

		// With Vulkan it's possible that in your swap chain becomes invalid or 
		// unoptimized while your application is running, for example because 
		// the window was resized. In that case the swap chain actually needs 
		// to be recreated from scratch and a reference to the old one must be 
		// specified in this field.
		swapChainCreateInfo.oldSwapchain = VK_NULL_HANDLE;

		if (vkCreateSwapchainKHR(logicalDevice, &swapChainCreateInfo, nullptr, &swapChain) != VK_SUCCESS)
		{
			throw std::runtime_error("Failed to create swap chain!");
		}

		vkGetSwapchainImagesKHR(logicalDevice, swapChain, &imageCount, nullptr);
		swapChainImages.resize(imageCount);
		vkGetSwapchainImagesKHR(logicalDevice, swapChain, &imageCount, swapChainImages.data());

		swapChainImageFormat = surfaceFormat.format;
		swapChainExtent = extent;
	}

	void VulkanSystem::createImageViews()
	{
		swapChainImageViews.resize(swapChainImages.size());

		for (size_t i = 0; i < swapChainImages.size(); i++)
		{
			VkImageViewCreateInfo imageViewCreateInfo{ };
			imageViewCreateInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
			imageViewCreateInfo.image = gsl::at(swapChainImages, i);

			imageViewCreateInfo.viewType = VK_IMAGE_VIEW_TYPE_2D;
			imageViewCreateInfo.format = swapChainImageFormat;

			// Do not swizzle any components
			imageViewCreateInfo.components.r = VK_COMPONENT_SWIZZLE_IDENTITY;
			imageViewCreateInfo.components.g = VK_COMPONENT_SWIZZLE_IDENTITY;
			imageViewCreateInfo.components.b = VK_COMPONENT_SWIZZLE_IDENTITY;
			imageViewCreateInfo.components.a = VK_COMPONENT_SWIZZLE_IDENTITY;

			// Describe the image purpose and mimap/layers
			imageViewCreateInfo.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
			imageViewCreateInfo.subresourceRange.baseMipLevel = 0;
			imageViewCreateInfo.subresourceRange.levelCount = 1;
			imageViewCreateInfo.subresourceRange.baseArrayLayer = 0;
			imageViewCreateInfo.subresourceRange.layerCount = 1;

			if (vkCreateImageView(logicalDevice, &imageViewCreateInfo, nullptr, &gsl::at(swapChainImageViews, i)) != VK_SUCCESS)
			{
				throw std::runtime_error("failed to create image views!");
			}
		}
	}

	void VulkanSystem::createRenderPass()
	{
		VkAttachmentDescription colorAttachment{ };
		colorAttachment.format = swapChainImageFormat;
		// TODO: Enable multisampling
		colorAttachment.samples = VK_SAMPLE_COUNT_1_BIT;

		// _LOAD, _CLEAR, or _DONT_CARE
		colorAttachment.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
		// _STORE, or _DONT_CARE
		colorAttachment.storeOp = VK_ATTACHMENT_STORE_OP_STORE;

		colorAttachment.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
		colorAttachment.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;

		// _COLOR_ATTACHMENT_OPTIMAL, _PRESENT_SRC_KHR, or _TRANSFER_DST_OPTIMAL
		colorAttachment.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
		colorAttachment.finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;

		// Setup render subpasses (post-processing, etc.)
		// Each subpass needs an attachmentReference
		VkAttachmentReference colorAttachmentRef{ };
		colorAttachmentRef.attachment = 0; // reference attachment by index
		colorAttachmentRef.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

		VkSubpassDescription subpass{ };
		// explicitly be a graphics subpass, not a compute
		subpass.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;

		// directly reference: layout(location = 0) out vec4 outColor
		subpass.colorAttachmentCount = 1;
		subpass.pColorAttachments = &colorAttachmentRef;

		// other available options: 
			// pInputAttachments - read from a shader
			// pResolveAttachments - multisampling
			// pDepthStencilAttachments - depth/stencil data
			// pPreserveAttachments - not used by this pass, but preserve


		// set up render subpass dependencies
		// with our ONE subpass and the two implicit External subpasses

		VkSubpassDependency dependency{ };
		// VK_SUBPASS_EXTERNAL refers to the implicit subpass either before or after the render pass
		// depending on whether its in src or dst
		// 0 refers to our subpass indices (our one and only subpass)
		dependency.srcSubpass = VK_SUBPASS_EXTERNAL;
		dependency.dstSubpass = 0;

		// Specify operations to wait on and which stages they occur in
		dependency.srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
		dependency.srcAccessMask = 0;

		// Specify which operations need to wait
		dependency.dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
		dependency.dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_READ_BIT | VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;

		VkRenderPassCreateInfo renderPassInfo{ };
		renderPassInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
		renderPassInfo.attachmentCount = 1;
		renderPassInfo.pAttachments = &colorAttachment;
		renderPassInfo.subpassCount = 1;
		renderPassInfo.pSubpasses = &subpass;
		renderPassInfo.dependencyCount = 1;
		renderPassInfo.pDependencies = &dependency;

		if (vkCreateRenderPass(logicalDevice, &renderPassInfo, nullptr, &renderPass) != VK_SUCCESS)
		{
			throw std::runtime_error("failed to create render pass!");
		}
	}

	void VulkanSystem::createGraphicsPipeline()
	{
		auto vertShaderCode = PP::Utils::Filesystem::readFile("./Triangle/triangle.vert.spv");
		auto fragShaderCode = PP::Utils::Filesystem::readFile("./Triangle/triangle.frag.spv");

		const auto vertShaderModule = createShaderModule(vertShaderCode);
		const auto fragShaderModule = createShaderModule(fragShaderCode);

		VkPipelineShaderStageCreateInfo vertShaderStageInfo{ };
		vertShaderStageInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
		vertShaderStageInfo.stage = VK_SHADER_STAGE_VERTEX_BIT;
		vertShaderStageInfo.module = vertShaderModule;
		vertShaderStageInfo.pName = "main";

		// shaders may want to use .pSpecializationInfo
		// in which we can set compile-time constants for the shader
		vertShaderStageInfo.pSpecializationInfo = nullptr;

		VkPipelineShaderStageCreateInfo fragShaderStageInfo{ };
		fragShaderStageInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
		fragShaderStageInfo.stage = VK_SHADER_STAGE_FRAGMENT_BIT;
		fragShaderStageInfo.module = fragShaderModule;
		fragShaderStageInfo.pName = "main";

		const VkPipelineShaderStageCreateInfo shaderStages[] = { vertShaderStageInfo, fragShaderStageInfo };

		// following tutorial, do not load vertex data, will be specified in the shader
		VkPipelineVertexInputStateCreateInfo vertexInputState{ };
		vertexInputState.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;
		vertexInputState.vertexBindingDescriptionCount = 0;
		vertexInputState.pVertexBindingDescriptions = nullptr; // Optional
		vertexInputState.vertexAttributeDescriptionCount = 0;
		vertexInputState.pVertexAttributeDescriptions = nullptr; // Optional

		VkPipelineInputAssemblyStateCreateInfo inputAssemblyState{ };
		inputAssemblyState.sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
		// topology options: https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPrimitiveTopology.html
		inputAssemblyState.topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
		// primitive restart allows you to separate strips using a special character like 0xFFFFFFFFFF
		inputAssemblyState.primitiveRestartEnable = VK_FALSE;

		VkViewport viewport{ };
		viewport.x = 0.0f;
		viewport.y = 0.0f;
		viewport.width = static_cast<float>(swapChainExtent.width);
		viewport.height = static_cast<float>(swapChainExtent.height);
		viewport.minDepth = 0.0f;
		viewport.maxDepth = 1.0f;

		VkRect2D scissor = {};
		scissor.offset = { 0, 0 };
		scissor.extent = swapChainExtent;

		// could use multiple viewports/scissors, requires enabling the feature in logical device creation
		VkPipelineViewportStateCreateInfo viewportState{ };
		viewportState.sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
		viewportState.viewportCount = 1;
		viewportState.pViewports = &viewport;
		viewportState.scissorCount = 1;
		viewportState.pScissors = &scissor;

		VkPipelineRasterizationStateCreateInfo rasterizationState{ };
		rasterizationState.sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;

		// True clamps, rather than discarding, requires GPU feature
		rasterizationState.depthClampEnable = VK_FALSE;

		// True disables any output to the framebuffer
		rasterizationState.rasterizerDiscardEnable = VK_FALSE;

		// _FILL, _LINE, or _POINT, non-_FILL requires GPU feature.
		rasterizationState.polygonMode = VK_POLYGON_MODE_FILL;

		// >1.0 requires wideLines GPU feature.
		rasterizationState.lineWidth = 1.0f;

		rasterizationState.cullMode = VK_CULL_MODE_BACK_BIT; // NONE, _FRONT, _BACK, _BOTH
		rasterizationState.frontFace = VK_FRONT_FACE_CLOCKWISE;

		rasterizationState.depthBiasEnable = VK_FALSE;
		rasterizationState.depthBiasConstantFactor = 0.0f; // Optional
		rasterizationState.depthBiasClamp = 0.0f; // Optional
		rasterizationState.depthBiasSlopeFactor = 0.0f; // Optional

		// Configuring Anti-Aliasing
		// TODO: Actually do AA, as per https://github.com/SaschaWillems/Vulkan/blob/master/multisampling/multisampling.cpp
		VkPipelineMultisampleStateCreateInfo multisampleState{ };
		multisampleState.sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
		multisampleState.sampleShadingEnable = VK_FALSE;
		multisampleState.rasterizationSamples = VK_SAMPLE_COUNT_1_BIT;
		multisampleState.minSampleShading = 1.0f; // Optional
		multisampleState.pSampleMask = nullptr; // Optional
		multisampleState.alphaToCoverageEnable = VK_FALSE; // Optional
		multisampleState.alphaToOneEnable = VK_FALSE; // Optional

		// TODO: Add depth and stencil testing

		// Color blending
		// OPT 1: Mix old and new color
		// OPT 2: Combine old and new using a bitwise operation

		// AttachmentState configures blend per framebuffer
		// ColorBlendStateCreateInfo configures global color blending
		VkPipelineColorBlendAttachmentState colorBlendAttachment{ };
		colorBlendAttachment.colorWriteMask = VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT;

		// the following does Alpha Blending:
		// finalColor.rgb = newAlpha * newColor + (1 - newAlpha) * oldColor;
		// finalColor.a = newAlpha.a;
		colorBlendAttachment.blendEnable = VK_TRUE;
		colorBlendAttachment.srcColorBlendFactor = VK_BLEND_FACTOR_SRC_ALPHA;
		colorBlendAttachment.dstColorBlendFactor = VK_BLEND_FACTOR_ONE_MINUS_SRC_ALPHA;
		colorBlendAttachment.colorBlendOp = VK_BLEND_OP_ADD;
		colorBlendAttachment.srcAlphaBlendFactor = VK_BLEND_FACTOR_ONE;
		colorBlendAttachment.dstAlphaBlendFactor = VK_BLEND_FACTOR_ZERO;
		colorBlendAttachment.alphaBlendOp = VK_BLEND_OP_ADD;

		VkPipelineColorBlendStateCreateInfo colorBlendingState{ };
		colorBlendingState.sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
		colorBlendingState.logicOpEnable = VK_FALSE;
		colorBlendingState.logicOp = VK_LOGIC_OP_COPY; // Optional
		colorBlendingState.attachmentCount = 1;
		colorBlendingState.pAttachments = &colorBlendAttachment;
		colorBlendingState.blendConstants[0] = 0.0f; // Optional
		colorBlendingState.blendConstants[1] = 0.0f; // Optional
		colorBlendingState.blendConstants[2] = 0.0f; // Optional
		colorBlendingState.blendConstants[3] = 0.0f; // Optional

		// TODO: Dynamic state here - https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPipelineDynamicStateCreateInfo.html

		// setup pipeline layout
		VkPipelineLayoutCreateInfo pipelineLayoutCreateInfo{ };
		pipelineLayoutCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
		pipelineLayoutCreateInfo.setLayoutCount = 0; // Optional
		pipelineLayoutCreateInfo.pSetLayouts = nullptr; // Optional
		pipelineLayoutCreateInfo.pushConstantRangeCount = 0; // Optional
		pipelineLayoutCreateInfo.pPushConstantRanges = nullptr; // Optional

		if (vkCreatePipelineLayout(logicalDevice, &pipelineLayoutCreateInfo, nullptr, &pipelineLayout) != VK_SUCCESS)
		{
			throw std::runtime_error("failed to create pipeline layout!");
		}

		VkGraphicsPipelineCreateInfo pipelineCreateInfo{ };
		pipelineCreateInfo.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
		pipelineCreateInfo.stageCount = 2;
		pipelineCreateInfo.pStages = &*shaderStages;

		pipelineCreateInfo.pVertexInputState = &vertexInputState;
		pipelineCreateInfo.pInputAssemblyState = &inputAssemblyState;
		pipelineCreateInfo.pViewportState = &viewportState;
		pipelineCreateInfo.pRasterizationState = &rasterizationState;
		pipelineCreateInfo.pMultisampleState = &multisampleState;
		pipelineCreateInfo.pDepthStencilState = nullptr; // Optional
		pipelineCreateInfo.pColorBlendState = &colorBlendingState;
		pipelineCreateInfo.pDynamicState = nullptr; // Optional

		pipelineCreateInfo.layout = pipelineLayout;

		pipelineCreateInfo.renderPass = renderPass;
		pipelineCreateInfo.subpass = 0; // index of subpass where this graphics pass is to be used

		// pipeline derivatives (child-pipelines) are created via this:
		// existing pipelines (via handle):
		pipelineCreateInfo.basePipelineHandle = VK_NULL_HANDLE; // Optional
		// about to exist pipelines (via index):
		pipelineCreateInfo.basePipelineIndex = -1; // Optional

		if (vkCreateGraphicsPipelines(logicalDevice, VK_NULL_HANDLE, 1, &pipelineCreateInfo, nullptr, &graphicsPipeline) != VK_SUCCESS)
		{
			throw std::runtime_error("failed to create graphics pipeline!");
		}

		vkDestroyShaderModule(logicalDevice, fragShaderModule, nullptr);
		vkDestroyShaderModule(logicalDevice, vertShaderModule, nullptr);
	}

	void VulkanSystem::createFramebuffers()
	{
		swapChainFramebuffers.resize(swapChainImageViews.size());

		for (size_t i = 0; i < swapChainImageViews.size(); i++)
		{
			VkFramebufferCreateInfo framebufferInfo{ };
			framebufferInfo.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
			framebufferInfo.renderPass = renderPass;
			framebufferInfo.attachmentCount = 1;
			framebufferInfo.pAttachments = &gsl::at(swapChainImageViews, i);
			framebufferInfo.width = swapChainExtent.width;
			framebufferInfo.height = swapChainExtent.height;
			framebufferInfo.layers = 1;

			if (vkCreateFramebuffer(logicalDevice, &framebufferInfo, nullptr, &gsl::at(swapChainFramebuffers, i)) != VK_SUCCESS)
			{
				throw std::runtime_error("failed to create framebuffer!");
			}
		}
	}

	void VulkanSystem::createCommandPool()
	{
		const auto queueFamilyIndices = findQueueFamilies(physicalDevice);

		VkCommandPoolCreateInfo poolInfo{ };
		poolInfo.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
		poolInfo.queueFamilyIndex = queueFamilyIndices.graphicsFamily;

		// VK_COMMAND_POOL _CREATE_TRANSIENT_BIT | _CREATE_RESET_COMMAND_BUFFER_BIT
		// First flag indicates command buffers will be rerecorded often
		// Second flag allows command buffers to be rerecorded individually
		poolInfo.flags = 0; // Optional

		if (vkCreateCommandPool(logicalDevice, &poolInfo, nullptr, &commandPool) != VK_SUCCESS)
		{
			throw std::runtime_error("failed to create command pool!");
		}
	}

	void VulkanSystem::createCommandBuffers()
	{
		commandBuffers.resize(swapChainFramebuffers.size());

		VkCommandBufferAllocateInfo allocInfo{ };
		allocInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
		allocInfo.commandPool = commandPool;

		// _PRIMARY allows command buffer to be queued for execution
		// _SECONDARY allows command buffer to be called from _PRIMARY ones, but not queued
		allocInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
		allocInfo.commandBufferCount = gsl::narrow<uint32>(commandBuffers.size());

		if (vkAllocateCommandBuffers(logicalDevice, &allocInfo, commandBuffers.data()) != VK_SUCCESS)
		{
			throw std::runtime_error("failed to allocate command buffers!");
		}

		const VkClearValue clearColor{{{0.0f, 0.0f, 0.0f, 1.0f}}};

		for (size_t i = 0; i < commandBuffers.size(); i++)
		{
			VkCommandBufferBeginInfo beginInfo{ };
			beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;

			// Flags:
			// VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT - execute once, then re-record
			// VK_COMMAND_BUFFER_USAGE_RENDER_PASS_CONTINUE_BIT - secondary buffer used within one render pass
			// VK_COMMAND_BUFFER_USAGE_SIMULTANEOUS_USE_BIT - can be resubmitted whilst already executing
			beginInfo.flags = VK_COMMAND_BUFFER_USAGE_SIMULTANEOUS_USE_BIT;

			// for _SECONDARY buffers, which state to inherit from _PRIMARY ones
			beginInfo.pInheritanceInfo = nullptr; // Optional

			vkBeginCommandBuffer(commandBuffers[i], &beginInfo);

			VkRenderPassBeginInfo renderPassInfo{ };
			renderPassInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
			renderPassInfo.renderPass = renderPass;
			renderPassInfo.framebuffer = swapChainFramebuffers[i];

			// renderArea should match the size of the attachments to maximise performance
			renderPassInfo.renderArea.offset = { 0, 0 };
			renderPassInfo.renderArea.extent = swapChainExtent;

			renderPassInfo.clearValueCount = 1;
			renderPassInfo.pClearValues = &clearColor;

			// _INLINE if _PRIMARY
			// else _SECONDARY_COMMAND_BUFFERS
			vkCmdBeginRenderPass(commandBuffers[i], &renderPassInfo, VK_SUBPASS_CONTENTS_INLINE);

			// _COMPUTE or _GRAPHICS
			vkCmdBindPipeline(commandBuffers[i], VK_PIPELINE_BIND_POINT_GRAPHICS, graphicsPipeline);

			// vertexCount, instanceCount (instanced rendering), firstVertex, firstInstance
			vkCmdDraw(commandBuffers[i], 3, 1, 0, 0);

			vkCmdEndRenderPass(commandBuffers[i]);

			if (vkEndCommandBuffer(commandBuffers[i]) != VK_SUCCESS)
			{
				throw std::runtime_error("failed to record command buffer!");
			}
		}
	}

	void VulkanSystem::createSemaphores()
	{
		VkSemaphoreCreateInfo semaphoreInfo{ };
		semaphoreInfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;

		if (vkCreateSemaphore(logicalDevice, &semaphoreInfo, nullptr, &imageAvailableSemaphore) != VK_SUCCESS ||
			vkCreateSemaphore(logicalDevice, &semaphoreInfo, nullptr, &renderFinishedSemaphore) != VK_SUCCESS)
		{
			throw std::runtime_error("failed to create semaphores!");
		}


	}

	/* Utility */
	int VulkanSystem::rateDeviceSuitability(const VkPhysicalDevice& device) const
	{
		// TODO: Learn what Properties and Features I care about and score them
		VkPhysicalDeviceProperties deviceProperties;
		vkGetPhysicalDeviceProperties(device, &deviceProperties);
		VkPhysicalDeviceFeatures deviceFeatures;
		vkGetPhysicalDeviceFeatures(device, &deviceFeatures);

		// TODO: consider passing properties and features through.
		if (!isDeviceSuitable(device))
		{
			return 0;
		}

		auto score = 0;

		// Discrete GPUs have a significant performance advantage
		if (deviceProperties.deviceType == VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU)
		{
			score += 1000;
		}

		// Maximum possible size of textures affects graphics quality
		score += deviceProperties.limits.maxImageDimension2D;

		return score;
	}

	bool VulkanSystem::isDeviceSuitable(const VkPhysicalDevice& device) const
	{
		// TODO: Learn what Properties and Features I absolutely must have return false if unavailable
		VkPhysicalDeviceProperties deviceProperties;
		vkGetPhysicalDeviceProperties(device, &deviceProperties);
		VkPhysicalDeviceFeatures deviceFeatures;
		vkGetPhysicalDeviceFeatures(device, &deviceFeatures);

		// Application can't function without geometry shaders
		if (!deviceFeatures.geometryShader)
		{
			return false;
		}

		const auto indices = findQueueFamilies(device);
		const auto extensionsSupported = checkDeviceExtensionSupport(device);

		// Short-circuit to protect swapChainAdequate (Expects(extensionsSupported))
		return indices.isComplete() && extensionsSupported && swapChainAdequate(device);
	}

	QueueFamilyIndices VulkanSystem::findQueueFamilies(const VkPhysicalDevice& device) const
	{
		QueueFamilyIndices indices;

		uint32_t queueFamilyCount;
		vkGetPhysicalDeviceQueueFamilyProperties(device, &queueFamilyCount, nullptr);

		std::vector<VkQueueFamilyProperties> queueFamilies(queueFamilyCount);
		vkGetPhysicalDeviceQueueFamilyProperties(device, &queueFamilyCount, queueFamilies.data());

		auto i = 0;
		for (const auto& queueFamily : queueFamilies)
		{
			VkBool32 presentSupport = false;
			vkGetPhysicalDeviceSurfaceSupportKHR(device, i, surface, &presentSupport);

			// bias queueFamilies that do both, not just one.
			if (queueFamily.queueCount > 0 && queueFamily.queueFlags & VK_QUEUE_GRAPHICS_BIT && presentSupport)
			{
				indices.graphicsFamily = i;
				indices.presentFamily = i;
			}
			else if (queueFamily.queueCount > 0 && queueFamily.queueFlags & VK_QUEUE_GRAPHICS_BIT)
			{
				indices.graphicsFamily = i;
			}
			else if (queueFamily.queueCount > 0 && presentSupport)
			{
				indices.presentFamily = i;
			}

			if (indices.isComplete())
			{
				break;
			}

			i++;
		}

		return indices;
	}

	SwapChainSupportDetails VulkanSystem::querySwapChainSupport(const VkPhysicalDevice& device) const
	{
		SwapChainSupportDetails details;

		vkGetPhysicalDeviceSurfaceCapabilitiesKHR(device, surface, &details.capabilities);

		uint32_t formatCount;
		vkGetPhysicalDeviceSurfaceFormatsKHR(device, surface, &formatCount, nullptr);

		if (formatCount != 0)
		{
			details.formats.resize(formatCount);
			vkGetPhysicalDeviceSurfaceFormatsKHR(device, surface, &formatCount, details.formats.data());
		}

		uint32_t presentModeCount;
		vkGetPhysicalDeviceSurfacePresentModesKHR(device, surface, &presentModeCount, nullptr);

		if (presentModeCount != 0)
		{
			details.presentModes.resize(presentModeCount);
			vkGetPhysicalDeviceSurfacePresentModesKHR(device, surface, &presentModeCount, details.presentModes.data());
		}

		return details;
	}

	bool VulkanSystem::swapChainAdequate(const VkPhysicalDevice& device) const
	{
		Expects(checkDeviceExtensionSupport(device));

		auto swapChainSupport = querySwapChainSupport(device);
		return !swapChainSupport.formats.empty() && !swapChainSupport.presentModes.empty();
	}

	VkExtent2D VulkanSystem::chooseSwapExtent(const VkSurfaceCapabilitiesKHR& capabilities) const noexcept
	{
		if (capabilities.currentExtent.width != std::numeric_limits<uint32_t>::max())
		{
			return capabilities.currentExtent;
		}
		VkExtent2D actualExtent{ WIDTH, HEIGHT };

		actualExtent.width = std::max(capabilities.minImageExtent.width, std::min(capabilities.maxImageExtent.width, actualExtent.width));
		actualExtent.height = std::max(capabilities.minImageExtent.height, std::min(capabilities.maxImageExtent.height, actualExtent.height));

		return actualExtent;
	}

	VkShaderModule VulkanSystem::createShaderModule(const std::vector<char>& shaderByteCode)
	{
		VkShaderModuleCreateInfo createInfo{ };
		createInfo.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
		createInfo.codeSize = shaderByteCode.size();

		// convert from char size data to uint32_t size data
		std::vector<uint32_t> shaderByteCodeAligned(shaderByteCode.size() / sizeof(uint32_t) + 1);
		memcpy(shaderByteCodeAligned.data(), shaderByteCode.data(), shaderByteCode.size());
		createInfo.pCode = shaderByteCodeAligned.data();

		VkShaderModule shaderModule;
		if (vkCreateShaderModule(logicalDevice, &createInfo, nullptr, &shaderModule) != VK_SUCCESS)
		{
			throw std::runtime_error("failed to create shader module!");
		}

		return shaderModule;
	}

	/* Static */
	requirementsSupportedResult VulkanSystem::checkValidationLayerSupport()
	{
		uint32_t layerCount;
		vkEnumerateInstanceLayerProperties(&layerCount, nullptr);

		std::vector<VkLayerProperties> availableLayers(layerCount);
		vkEnumerateInstanceLayerProperties(&layerCount, availableLayers.data());

		requirementsSupportedResult result = { true, { } };

		for (auto layerName : validationLayers)
		{
			auto layerFound = false;

			for (const auto& layerProperties : availableLayers)
			{
				if (strcmp(layerName, layerProperties.layerName) == 0)
				{
					layerFound = true;
					break;
				}
			}

			if (!layerFound)
			{
				result.allSupported = false;
				result.missingRequirements.push(layerName);
			}
		}

		return result;
	}

	std::vector<const char*> VulkanSystem::getRequiredInstanceExtensions()
	{
		std::vector<const char*> instanceExtensions;

#if PP_PLATFORM_SYSTEM == PP_PLATFORM_GLFW
		uint32 glfwExtensionCount;
		const char** glfwExtensionsRaw = glfwGetRequiredInstanceExtensions(&glfwExtensionCount);

		gsl::span<const char*> glfwExtensions{ glfwExtensionsRaw, gsl::narrow <std::size_t>(glfwExtensionCount) };
		for (const auto extension : glfwExtensions)
		{
			instanceExtensions.push_back(extension);
		}
#else
		PP_NOT_IMPLEMENTED;
#endif

		if (enableValidationLayers)
		{
			instanceExtensions.push_back(VK_EXT_DEBUG_REPORT_EXTENSION_NAME);
		}

		return instanceExtensions;
	}

	bool VulkanSystem::checkDeviceExtensionSupport(const VkPhysicalDevice& device)
	{
		uint32_t extensionCount = 0;
		vkEnumerateDeviceExtensionProperties(device, nullptr, &extensionCount, nullptr);

		std::vector<VkExtensionProperties> availableExtensions(extensionCount);
		vkEnumerateDeviceExtensionProperties(device, nullptr, &extensionCount, availableExtensions.data());

		std::set<std::string> requiredExtensions(deviceExtensions.begin(), deviceExtensions.end());

		// TODO: look at replacing other nested loops with std::set.erase()
		for (const auto& extension : availableExtensions)
		{
			requiredExtensions.erase(extension.extensionName);
		}

		return requiredExtensions.empty();
	}

	VkSurfaceFormatKHR VulkanSystem::chooseSwapSurfaceFormat(const std::vector<VkSurfaceFormatKHR>& availableFormats) noexcept
	{
		// If the surface has no preferred format:
		if (availableFormats.size() == 1
			&& availableFormats[0].format == VK_FORMAT_UNDEFINED)
		{
			return { VK_FORMAT_B8G8R8A8_UNORM, VK_COLOR_SPACE_SRGB_NONLINEAR_KHR };
		}

		// Otherwise, if it has a list of formats, find if it has the combination we want:
		for (const auto& availableFormat : availableFormats)
		{
			if (availableFormat.format == VK_FORMAT_B8G8R8A8_UNORM
				&& availableFormat.colorSpace == VK_COLOR_SPACE_SRGB_NONLINEAR_KHR)
			{
				return availableFormat;
			}
		}

		// TODO: Consider adding a ranking system for formats that don't match
		// For now, just give the first available one
		return availableFormats[0];
	}

	VkPresentModeKHR VulkanSystem::chooseSwapPresentMode(const std::vector<VkPresentModeKHR> availablePresentModes) noexcept
	{
		// available options (in order from least to most preferred):
		// 2 VK_PRESENT_MODE_FIFO_KHR - queue buffer, can cause block if queue is full, guaranteed to be available
		// 0 VK_PRESENT_MODE_IMMEDIATE_KHR - no buffering, can cause screen tear
		// 3 VK_PRESENT_MODE_FIFO_RELAXED_KHR - queue buffer, can display immediately if queue is empty upon arrival, can cause screen tear
		// TODO: See if we should add this before IMMEDIATE
		// 1 VK_PRESENT_MODE_MAILBOX_KHR - queue buffer, can replace older images if queue is full upon arrival, also can do triple buffering

		auto bestMode = VK_PRESENT_MODE_FIFO_KHR;

		// We don't know what order the available present modes will appear in
		for (const auto& availablePresentMode : availablePresentModes)
		{
			// If the least preferred
			if (bestMode == VK_PRESENT_MODE_FIFO_KHR)
			{
				// Accept any available mode
				if (availablePresentMode == VK_PRESENT_MODE_MAILBOX_KHR)
				{
					bestMode = availablePresentMode;
				}
				else if (availablePresentMode == VK_PRESENT_MODE_FIFO_RELAXED_KHR)
				{
					bestMode = availablePresentMode;
				}
				else if (availablePresentMode == VK_PRESENT_MODE_IMMEDIATE_KHR)
				{
					bestMode = availablePresentMode;
				}
			}
			// If the 2nd least preferred
			else if (bestMode == VK_PRESENT_MODE_IMMEDIATE_KHR)
			{
				// Accept anything better
				if (availablePresentMode == VK_PRESENT_MODE_MAILBOX_KHR)
				{
					bestMode = availablePresentMode;
				}
				else if (availablePresentMode == VK_PRESENT_MODE_FIFO_RELAXED_KHR)
				{
					bestMode = availablePresentMode;
				}
			}
			// If the 3rd least preferred
			else if (availablePresentMode == VK_PRESENT_MODE_FIFO_RELAXED_KHR)
			{
				// Accept only the best
				if (availablePresentMode == VK_PRESENT_MODE_MAILBOX_KHR)
				{
					bestMode = availablePresentMode;
				}
			}
		}

		return bestMode;
	}

	/* Callbacks */
	VkBool32 VulkanSystem::debugCallback(VkDebugReportFlagsEXT flags, VkDebugReportObjectTypeEXT objType, uint64_t obj, size_t location, int32_t code, const char* layerPrefix, const char* msg, void* userData)
	{
		/* flags =
		VK_DEBUG_REPORT_INFORMATION_BIT_EXT
		VK_DEBUG_REPORT_WARNING_BIT_EXT
		VK_DEBUG_REPORT_PERFORMANCE_WARNING_BIT_EXT
		VK_DEBUG_REPORT_ERROR_BIT_EXT
		VK_DEBUG_REPORT_DEBUG_BIT_EXT
		*/

		std::cerr << "Validation layer: " << msg << std::endl;

		return VK_FALSE;
	}
}