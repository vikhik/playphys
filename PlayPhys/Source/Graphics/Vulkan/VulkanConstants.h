#pragma once

#include <vulkan/vulkan.h>

namespace PP::Graphics
{
	constexpr int WIDTH = 800;
	constexpr int HEIGHT = 600;

	constexpr std::array<const char*, 1> validationLayers =
	{
		"VK_LAYER_KHRONOS_validation",
	};

	constexpr std::array<const char*, 1> deviceExtensions =
	{
		VK_KHR_SWAPCHAIN_EXTENSION_NAME
	};

	constexpr bool enableValidationLayers = PP_GRAPHICS_VALIDATION;
}