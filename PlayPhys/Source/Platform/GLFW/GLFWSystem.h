#pragma once

#include "../PlatformSystem.h"

#include <stdexcept>

// Forward declare GLFWwindow to avoid including glfw3.h
struct GLFWwindow;

namespace PP::Platform
{
	class GLFWWindow : public IWindow
	{
	public:
		void init(Settings settings) override;

		// actual glfw window handle
		GLFWwindow* window;
	};

	class GLFWSystem : public PlatformSystem
	{
	public:
		void init() override;
		void run() override;
		void cleanup() override;
		bool getShouldClose() override;

	private:
		GLFWWindow& getWindow() { return *static_cast<GLFWWindow*>(window.get()); }
	};
}