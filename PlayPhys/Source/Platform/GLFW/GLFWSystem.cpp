#include "enginepch.h"

#include <GLFW/glfw3.h>

#include "GLFWSystem.h"

namespace PP::Platform
{
	void GLFWWindow::init(Settings settings)
	{
		if (!glfwInit())
			throw new std::exception("glfw failed to initialize");

		// Tell GLFW to not create an OpenGL context
		glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);

		// Disable window resizing handling
		glfwWindowHint(GLFW_RESIZABLE, GLFW_FALSE);

		window = glfwCreateWindow(settings.width, settings.height, "PlayPhys - GLFW", nullptr, nullptr);
	}

	void GLFWSystem::init()
	{
		PROFILE(PlatformSystem, init);
		window = std::make_unique<GLFWWindow>();
		window->init({ 800, 600 });
	}

	void GLFWSystem::run()
	{
		PROFILE(PlatformSystem, run);
		glfwPollEvents();
	}

	void GLFWSystem::cleanup()
	{
		PROFILE(PlatformSystem, cleanup);
		glfwDestroyWindow(getWindow().window);

		glfwTerminate();
	}

	bool GLFWSystem::getShouldClose()
	{
		return glfwWindowShouldClose(getWindow().window);
	}
}
