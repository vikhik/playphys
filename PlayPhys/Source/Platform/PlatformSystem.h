#pragma once

#include <memory>
#include "Utils/Types.h"

namespace PP::Platform
{
	class IWindow
	{
	public:
		IWindow() = default;
		virtual ~IWindow() {};

		struct Settings
		{
			uint16 width;
			uint16 height;
		};

		virtual void init(Settings settings) = 0;
	};

	class PlatformSystem
	{
	public:
		PlatformSystem() noexcept = default;
		PlatformSystem(const PlatformSystem&) = delete;
		PlatformSystem(PlatformSystem&&) = delete;
		PlatformSystem& operator=(const PlatformSystem&) = delete;
		PlatformSystem& operator=(PlatformSystem&&) = delete;
		virtual ~PlatformSystem() = default;

		virtual void init() = 0;
		virtual void run() = 0;
		virtual void cleanup() = 0;
		virtual bool getShouldClose() = 0;

		IWindow& getWindow() { return *window; }

	protected:
		std::unique_ptr<IWindow> window;
	};
}