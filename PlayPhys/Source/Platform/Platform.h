#pragma once

#include "PlatformSystem.h"
#include <memory>

#if PP_PLATFORM_SYSTEM == PP_PLATFORM_GLFW

#include "GLFW/GLFWSystem.h"

#else

PP_NOT_IMPLEMENTED;

#endif

namespace PP::Platform
{
	std::unique_ptr<PlatformSystem> getPlatformSystem();

#if PP_PLATFORM_SYSTEM == PP_PLATFORM_GLFW

	inline std::unique_ptr<PlatformSystem> getPlatformSystem()
	{
		return std::make_unique<GLFWSystem>();
	}

#endif
}