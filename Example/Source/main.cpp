#include "ExampleGame.h"

int main(const int argc, const char** argv)
{
	auto game = ExampleGame();
	return game.run(argc, argv);
}
