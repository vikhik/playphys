#include "enginepch.h"
#include "ExampleGame.h"

int ExampleGame::run(const int argc, const char** argv)
{
	using namespace PP;

	std::vector<std::string_view> args = Utils::CLI::readArgs(argc, argv);

	Game game = Game();
	size_t ticks{};

	game.init(args);

	while (!game.getShouldClose())
	{
		++ticks;
		game.run();
	}

	game.cleanup();

	if (ProfilerManager* profileManager = ProfilerManager::getInstance())
	{
		auto time = std::chrono::duration_cast<std::chrono::microseconds>(profileManager->getTime("Game", "run"));
		auto ms_per_frame = time / ticks / 1000.f;

		auto graphics_run = std::chrono::duration_cast<std::chrono::microseconds>(profileManager->getTime("GraphicsSystem", "run"));
		auto graphics_ms_per_frame = graphics_run / ticks / 1000.f;

		auto graphics_draw = std::chrono::duration_cast<std::chrono::microseconds>(profileManager->getTime("GraphicsSystem", "drawFrame"));
		auto graphics_draw_ms_per_frame = graphics_draw / ticks / 1000.f;

		auto platform_run = std::chrono::duration_cast<std::chrono::microseconds>(profileManager->getTime("PlatformSystem", "run"));
		auto platform_ms_per_frame = platform_run / ticks / 1000.f;

		std::cout << std::endl << "Over " << ticks << " frames the average time was " << std::setprecision(3) << ms_per_frame.count() << "ms per frame" << std::endl;
		std::cout << std::endl;
		std::cout << "* Rendering took " << std::setprecision(3) << graphics_ms_per_frame.count() << "ms per frame" << std::endl;
		std::cout << "  * of which " << std::setprecision(3) << graphics_draw_ms_per_frame.count() << "ms was drawing" << std::endl;
		std::cout << std::endl;
		std::cout << "* Platform took " << std::setprecision(3) << platform_ms_per_frame.count() << "ms per frame" << std::endl;
	}

	return EXIT_SUCCESS;
}
