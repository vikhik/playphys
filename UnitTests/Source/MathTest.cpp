#include "testpch.h"

#include "Math/Math.h"
#include "Utils/Types.h"

// Ideas for testing: https://sourceforge.net/p/ggt/code/HEAD/tree/tags/0.6.1/Test/TestSuite/TestCases/

namespace PP::Math
{
	TEST(Vector3f, Statics_Zero)
	{
		Vec3f vec{ 0, 0, 0 };

		EXPECT_EQ(vec, Vec3f::Zero);
	}

	TEST(Vector3f, Statics_ValueType)
	{
		static_assert(std::is_same_v<Vec3f::value_type, PP::real32>);
		static_assert(std::is_same_v<Vec3f::size_type, PP::uint32>);
	}

	TEST(Vector3f, Statics_Size)
	{
		Vec3f vec;

		EXPECT_EQ(sizeof(vec), 12);
	}

	TEST(Vector3f, Statics_Count)
	{
		EXPECT_EQ(Vec3f::count, 3);
	}

	TEST(Vector3f, Constructor_List_0)
	{
		Vec3f vec{ };

		EXPECT_EQ(vec[0], 0);
		EXPECT_EQ(vec[1], 0);
		EXPECT_EQ(vec[2], 0);

		EXPECT_EQ(vec.x, 0);
		EXPECT_EQ(vec.y, 0);
		EXPECT_EQ(vec.z, 0);

		EXPECT_EQ(vec.x, 0.f);
		EXPECT_EQ(vec.y, 0.f);
		EXPECT_EQ(vec.z, 0.f);
	}

	TEST(Vector3f, Constructor_List_1)
	{
		// Note: Vec3f{ 0 } is ambiguous with a float*
		Vec3f vec{ 1 };

		EXPECT_EQ(vec[0], 1);
		EXPECT_EQ(vec[1], 1);
		EXPECT_EQ(vec[2], 1);

		EXPECT_EQ(vec.x, 1);
		EXPECT_EQ(vec.y, 1);
		EXPECT_EQ(vec.z, 1);

		EXPECT_EQ(vec.x, 1.f);
		EXPECT_EQ(vec.y, 1.f);
		EXPECT_EQ(vec.z, 1.f);
	}

	TEST(Vector3f, Constructor_List_3)
	{
		Vec3f vec{ 1, 2, 3 };

		EXPECT_EQ(vec[0], 1);
		EXPECT_EQ(vec[1], 2);
		EXPECT_EQ(vec[2], 3);

		EXPECT_EQ(vec.x, 1);
		EXPECT_EQ(vec.y, 2);
		EXPECT_EQ(vec.z, 3);

		EXPECT_EQ(vec.x, 1.f);
		EXPECT_EQ(vec.y, 2.f);
		EXPECT_EQ(vec.z, 3.f);
	}

	TEST(Vector3f, Constructor_Copy)
	{
		Vec3f vecA{ 0, -1, 2 };

		auto vecB = vecA;

		EXPECT_EQ(vecB[0], 0);
		EXPECT_EQ(vecB[1], -1);
		EXPECT_EQ(vecB[2], 2);

		EXPECT_EQ(vecB.x, 0);
		EXPECT_EQ(vecB.y, -1);
		EXPECT_EQ(vecB.z, 2);
	}

	TEST(Vector3f, Constructor_From_Array)
	{
		float arr[3]{ 3, -1, 2 };
		auto* ptr = arr;
		Vec3f vecA{ ptr };

		EXPECT_EQ(vecA[0], 3);
		EXPECT_EQ(vecA[1], -1);
		EXPECT_EQ(vecA[2], 2);
	}

	TEST(Vector3f, Constructor_From_Span)
	{
		float arr[3]{ 3, -1, 2 };
		gsl::span<float, 3> span{ arr };
		Vec3f vecA{ span };

		EXPECT_EQ(vecA[0], 3);
		EXPECT_EQ(vecA[1], -1);
		EXPECT_EQ(vecA[2], 2);
	}

	TEST(Vector3f, Constructor_Vec2f_0)
	{
		Vec2f vec2{ 3, 4 };
		Vec3f vec3{ vec2 };

		EXPECT_EQ(vec3.x, 3);
		EXPECT_EQ(vec3.y, 4);
		EXPECT_EQ(vec3.z, 0);
	}

	TEST(Vector3f, Constructor_Vec2f_Z)
	{
		Vec2f vec2{ 3, 4 };
		Vec3f vec3{ vec2, 5 };

		EXPECT_EQ(vec3.x, 3);
		EXPECT_EQ(vec3.y, 4);
		EXPECT_EQ(vec3.z, 5);
	}

	TEST(Vector3f, Accessor_Index)
	{
		Vec3f vec{ };

		EXPECT_EQ(vec[0], 0);
		EXPECT_EQ(vec[1], 0);
		EXPECT_EQ(vec[2], 0);

		EXPECT_EQ(vec[0], 0.f);
		EXPECT_EQ(vec[1], 0.f);
		EXPECT_EQ(vec[2], 0.f);
	}

	TEST(Vector3f, Accessor_Member)
	{
		Vec3f vec{ };

		EXPECT_EQ(vec.x, 0);
		EXPECT_EQ(vec.y, 0);
		EXPECT_EQ(vec.z, 0);

		EXPECT_EQ(vec.r, 0);
		EXPECT_EQ(vec.g, 0);
		EXPECT_EQ(vec.b, 0);
	}

	TEST(Vector3f, Accessor_Const_Dereference)
	{
		// TODO: Verify what further dereference checks we need for interop with other libs
		Vec3f vec{ 1, 2, 3 };

		const auto ptr = *vec;

		EXPECT_EQ(ptr[0], 1);
		EXPECT_EQ(ptr[1], 2);
		EXPECT_EQ(ptr[2], 3);
	}

	TEST(Vector3f, Accessor_Non_Const_Dereference)
	{
		// TODO: Verify what further dereference checks we need for interop with other libs
		Vec3f vec{ 1, 2, 3 };

		auto ptr = *vec;

		EXPECT_EQ(ptr[0], 1);
		EXPECT_EQ(ptr[1], 2);
		EXPECT_EQ(ptr[2], 3);

		ptr[1] = 5;

		EXPECT_EQ(ptr[0], 1);
		EXPECT_EQ(ptr[1], 5);
		EXPECT_EQ(ptr[2], 3);

		EXPECT_EQ(vec[0], 1);
		EXPECT_EQ(vec[1], 5);
		EXPECT_EQ(vec[2], 3);
	}

	TEST(Vector3f, Equality)
	{
		Vec3f a{ 1, 1, 1 };
		Vec3f b{ 1 };

		EXPECT_TRUE(a == b);
		EXPECT_EQ(a, b);
	}

	TEST(Vector3f, Accessor_SubMembers)
	{
		Vec3f vec{ };

		EXPECT_EQ(vec.xy, Vec2f());
	}

	TEST(Vector3f, Assignment_Index)
	{
		Vec3f vec;

		vec[0] = 0;
		vec[1] = 1;
		vec[2] = 2;

		EXPECT_EQ(vec[0], 0);
		EXPECT_EQ(vec[1], 1);
		EXPECT_EQ(vec[2], 2);

		EXPECT_EQ(vec.x, 0);
		EXPECT_EQ(vec.y, 1);
		EXPECT_EQ(vec.z, 2);
	}

	TEST(Vector3f, Assignment_Member)
	{
		Vec3f vec;

		vec.x = 3;
		vec.y = 4;
		vec.z = 5;

		EXPECT_EQ(vec[0], 3);
		EXPECT_EQ(vec[1], 4);
		EXPECT_EQ(vec[2], 5);

		EXPECT_EQ(vec.x, 3);
		EXPECT_EQ(vec.y, 4);
		EXPECT_EQ(vec.z, 5);
	}

	TEST(Vector3f, Assignment_InitialiserList)
	{
		Vec3f vec;
		vec = { 3, 2, 1 };

		EXPECT_EQ(vec[0], 3);
		EXPECT_EQ(vec[1], 2);
		EXPECT_EQ(vec[2], 1);

		EXPECT_EQ(vec.x, 3);
		EXPECT_EQ(vec.y, 2);
		EXPECT_EQ(vec.z, 1);
	}

	TEST(Vector3f, Assignment_Copy)
	{
		Vec3f vecA{ 0, 0, 2 };
		Vec3f vecB{ 2, 3, 4 };

		vecA = vecB;

		EXPECT_EQ(vecA[0], 2);
		EXPECT_EQ(vecA[1], 3);
		EXPECT_EQ(vecA[2], 4);

		EXPECT_EQ(vecA.x, 2);
		EXPECT_EQ(vecA.y, 3);
		EXPECT_EQ(vecA.z, 4);
	}

	TEST(Vector3f, Func_Length)
	{
		Vec3f vec{ -1, -2, 3 };

		EXPECT_FLOAT_EQ(vec.length(), float(sqrt(1 + 4 + 9)));
	}

	TEST(Vector3f, Func_Magnitude)
	{
		Vec3f vec{ 1, 2, 3 };

		EXPECT_FLOAT_EQ(vec.magnitude(), float(sqrt(1 + 4 + 9)));
	}

	TEST(Vector3f, Func_Length_Squared)
	{
		Vec3f vec{ -1, -2, 3 };

		EXPECT_FLOAT_EQ(vec.lengthSquared(), float(1 + 4 + 9));
	}

	TEST(Vector3f, Func_Magnitude_Squared)
	{
		Vec3f vec{ 1, 2, 3 };

		EXPECT_FLOAT_EQ(vec.magnitudeSquared(), float(1 + 4 + 9));
	}

	TEST(Vector3f, Func_Min)
	{
		Vec3f vec{ 1, 2, 3 };

		EXPECT_FLOAT_EQ(vec.min(), float(1));
	}

	TEST(Vector3f, Func_Max)
	{
		Vec3f vec{ 1, 2, 3 };

		EXPECT_FLOAT_EQ(vec.max(), float(3));
	}

	TEST(Vector3f, Func_Abs)
	{
		Vec3f vec{ 1, -2, 3 };
		vec.abs();

		Vec3f absVec{ 1, 2, 3 };

		EXPECT_EQ(vec, absVec);
	}

	TEST(Vector3f, Func_Clamp)
	{
		Vec3f vec{ -1, -2, 3 };
		vec.clamp(1.5f, 2.2f);

		Vec3f clampedVec{ 1.5f, 1.5f, 2.2f };

		EXPECT_EQ(vec, clampedVec);
	}

	TEST(Vector3f, Func_Free_Dot)
	{
		Vec3f vecA{ 2 };
		Vec3f vecB{ 3 };

		EXPECT_EQ(dot(vecA, vecB), 18);
	}

	TEST(Vector3f, Func_Free_Abs)
	{
		Vec3f vec{ 1, -2, 3 };
		auto result = abs(vec);

		Vec3f absVec{ 1, 2, 3 };

		EXPECT_EQ(result, absVec);
		EXPECT_EQ(vec, Vec3f(1, -2, 3));
	}

	TEST(Vector3f, Func_Free_Clamp)
	{
		Vec3f vec{ -1, -2, 3 };
		auto result = clamp(vec, 1.5f, 2.2f);

		Vec3f clampedVec{ 1.5f, 1.5f, 2.2f };

		EXPECT_EQ(result, clampedVec);
		EXPECT_EQ(vec, Vec3f(-1, -2, 3));
	}

	TEST(Vector3f, Func_Free_Lerp)
	{
		Vec3f a{ 4, 5, 6 };
		Vec3f b{ 10 };
		Vec3f c{ -10 };

		// Verified using unity's implementation
		Vec3f ab50{ 7.f, 7.5f, 8.f };
		Vec3f ac75{ -6.5f, -6.25f, -6.f };

		EXPECT_EQ(lerp(a, b, .50f), ab50);
		EXPECT_EQ(lerp(a, c, .75f), ac75);
	}

	TEST(Vector3f, Operator_Unary_Subtract)
	{
		Vec3f vec{ 1, 2, 3 };

		auto vecSub{ -vec };

		EXPECT_EQ(vecSub[0], -1);
		EXPECT_EQ(vecSub[1], -2);
		EXPECT_EQ(vecSub[2], -3);
	}

	TEST(Vector3f, Operator_Infix_Scalar_Add)
	{
		Vec3f vec{ 1, 2, 3 };
		vec += 3;

		ASSERT_EQ(vec.x, 4);
		ASSERT_EQ(vec.y, 5);
		ASSERT_EQ(vec.z, 6);
	}

	TEST(Vector3f, Operator_Infix_Scalar_Sub)
	{
		Vec3f vec{ 1, 2, 3 };
		vec -= 3;

		ASSERT_EQ(vec.x, -2);
		ASSERT_EQ(vec.y, -1);
		ASSERT_EQ(vec.z, 0);
	}

	TEST(Vector3f, Operator_Infix_Scalar_Mul)
	{
		Vec3f vec{ 1, 2, 3 };
		vec *= 3;

		ASSERT_EQ(vec.x, 3);
		ASSERT_EQ(vec.y, 6);
		ASSERT_EQ(vec.z, 9);
	}

	TEST(Vector3f, Operator_Infix_Scalar_Div)
	{
		Vec3f vec{ 1, 2, 3 };
		vec /= 3;

		ASSERT_FLOAT_EQ(vec.x, 1.f / 3.f);
		ASSERT_FLOAT_EQ(vec.y, 2.f / 3.f);
		ASSERT_FLOAT_EQ(vec.z, 3.f / 3.f);
	}

	TEST(Vector3f, Operator_Infix_Vector_Add)
	{
		Vec3f a{ 1, 2, 3 };
		Vec3f b{ 2, 3, 4 };
		a += b;

		ASSERT_EQ(a.x, 3);
		ASSERT_EQ(a.y, 5);
		ASSERT_EQ(a.z, 7);
	}

	TEST(Vector3f, Operator_Infix_Vector_Sub)
	{
		Vec3f a{ 1, 2, 3 };
		Vec3f b{ 2, 3, 4 };
		a -= b;

		ASSERT_EQ(a.x, -1);
		ASSERT_EQ(a.y, -1);
		ASSERT_EQ(a.z, -1);
	}

	TEST(Vector3f, Operator_Infix_Vector_Mul)
	{
		Vec3f a{ 1, 2, 3 };
		Vec3f b{ 2, 3, 4 };
		a *= b;

		ASSERT_EQ(a.x, 2);
		ASSERT_EQ(a.y, 6);
		ASSERT_EQ(a.z, 12);
	}

	TEST(Vector3f, Operator_Infix_Vector_Div)
	{
		Vec3f a{ 1, 2, 3 };
		Vec3f b{ 2, 3, 4 };
		a /= b;

		ASSERT_FLOAT_EQ(a.x, 1.f / 2.f);
		ASSERT_FLOAT_EQ(a.y, 2.f / 3.f);
		ASSERT_FLOAT_EQ(a.z, 3.f / 4.f);
	}

	TEST(Vector3f, Operator_Binary_Scalar_Add)
	{
		Vec3f vec{ 1, 2, 3 };

		auto vecAdd = vec + 3;

		EXPECT_EQ(vecAdd[0], 4);
		EXPECT_EQ(vecAdd[1], 5);
		EXPECT_EQ(vecAdd[2], 6);
	}

	TEST(Vector3f, Operator_Binary_Scalar_Subtract)
	{
		Vec3f vec{ 1, 2, 3 };

		auto vecSub = vec - 3;

		EXPECT_EQ(vecSub[0], -2);
		EXPECT_EQ(vecSub[1], -1);
		EXPECT_EQ(vecSub[2], 0);
	}

	TEST(Vector3f, Operator_Binary_Scalar_Multiply)
	{
		Vec3f vec{ 3 };

		auto vecMul = vec * 3;

		EXPECT_EQ(vecMul[0], 9);
		EXPECT_EQ(vecMul[1], 9);
		EXPECT_EQ(vecMul[2], 9);
	}

	TEST(Vector3f, Operator_Binary_Scalar_Divide)
	{
		Vec3f vec{ 3 };

		auto vecDiv = vec / 3;

		EXPECT_EQ(vecDiv[0], 1);
		EXPECT_EQ(vecDiv[1], 1);
		EXPECT_EQ(vecDiv[2], 1);
	}

	TEST(Vector3f, Operator_Binary_Vector_Add)
	{
		Vec3f vecA{ 1, 2, 3 };
		Vec3f vecB{ 1, 3, 5 };

		auto vecAdd = vecA + vecB;

		EXPECT_EQ(vecAdd[0], 2);
		EXPECT_EQ(vecAdd[1], 5);
		EXPECT_EQ(vecAdd[2], 8);
	}

	TEST(Vector3f, Operator_Binary_Vector_Subtract)
	{
		Vec3f vecA{ 1, 2, 3 };
		Vec3f vecB{ 1, 3, 5 };

		auto vecAdd = vecA - vecB;

		EXPECT_EQ(vecAdd[0], 0);
		EXPECT_EQ(vecAdd[1], -1);
		EXPECT_EQ(vecAdd[2], -2);
	}

	TEST(Vector3f, Operator_Binary_Vector_Mul)
	{
		Vec3f vecA{ 2 };
		Vec3f vecB{ 3 };

		Vec3f expectedResult{ 6, 6, 6 };

		EXPECT_EQ(vecA * vecB, expectedResult);
	}

	TEST(Vector3f, Operator_Binary_Vector_Div)
	{
		Vec3f vecA{ 6 };
		Vec3f vecB{ 3 };

		Vec3f expectedResult{ 2, 2, 2 };

		EXPECT_EQ(vecA / vecB, expectedResult);
	}

	TEST(Vector3f, Operator_OStream)
	{
		std::ostringstream os{};
		Vec3f vec{ 1, 2, 3 };

		os << vec;

		EXPECT_EQ(os.str(), "(1, 2, 3)");
	}

	TEST(Vector3f, Operator_IStream)
	{
		std::istringstream os{ "(1, 2, 3)" };
		Vec3f vec;

		os >> vec;

		EXPECT_EQ(vec, Vec3f(1, 2, 3));
	}
}
