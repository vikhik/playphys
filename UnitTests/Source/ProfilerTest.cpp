#include "testpch.h"

#include <iostream>
#include <gtest/gtest.h>

#include "Profiling/Profiling.h"
#include "Utils/Defines.h"

namespace PP
{
	void printProfiler()
	{
		if (ProfilerManager::getInstance() != nullptr)
			std::cout << *ProfilerManager::getInstance();
	}

	void profileTest()
	{
		{
			PROFILE(ProfileProfiler, 1_Profile_Warmup);
			{
				PROFILE(ProfileProfiler, 2_Domain_Creation);
			}
			{
				PROFILE(ProfileProfiler, 3_Section_Creation);
			}
		}

		{
			{
				PROFILE(ProfilerCreation, 1_Domain_Creation);
			}

			{
				PROFILE(ProfilerCreation, 2_Section_Creation);
			}
		}

		{
			PROFILE(ProfileTest, Warm_Up);

			for (auto i = 0; i < 50; i++)
			{
				auto f = sqrt(i + i);
				f = ++f;
			}
		}

		{
			PROFILE(ProfileTest, Run_Once);

			for (auto i = 0; i < 1; i++)
			{
				auto f = sqrt(i + i);
				f = ++f;
			}
		}

		{
			PROFILE(ProfileTest, Run_50_Times_Once);

			for (auto i = 0; i < 50; i++)
			{
				auto f = sqrt(i + i);
				f = ++f;
			}
		}

		{
			PROFILE(ProfileTest, Run_50_Times_Twice);

			for (auto i = 0; i < 50; i++)
			{
				auto f = sqrt(i + i);
				f = ++f;
			}
		}

		{
			PROFILE(ProfileTest, Run_50_Times_Twice);

			for (auto i = 0; i < 50; i++)
			{
				auto f = sqrt(i + i);
				f = ++f;
			}
		}

		{
			PROFILE(ProfileTest, Run_5000_Times);

			for (auto i = 0; i < 5000; i++)
			{
				auto f = sqrt(i + i);
				f = ++f;
			}
		}

		{
			PROFILE(ProfileTest, Run_500000_Times);

			for (auto i = 0; i < 500000; i++)
			{
				auto f = sqrt(i + i);
				f = ++f;
			}
		}

		printProfiler();
	}

	TEST(Profiling, Performance)
	{
		if constexpr (PP_PROFILER_ENABLED)
			profileTest();
	}
}