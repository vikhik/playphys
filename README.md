# README

PlayPhys is a simple physics engine in C++, aiming to introduce basic multithreading, and possibly some GPGPU usage.

Version 0.0.0

## Development Environment

This project is built using VS 2019.
The development install options for this project are available in the adjacent [.vsconfig file](.vsconfig):

* Desktop Development with C++
* Game Development with C++
* Individual Component: NuGet Package Manager
* Individual Component: Test Adapter for Google Test

We also utilise the following [optional] extensions to Visual Studio 2017:

* [Visual Assist X](https://www.wholetomato.com/)

## Third Party Dependencies

We use the following third party libraries & SDKs.

### Required

The following are required to be downloaded to make use of the engine.

* Vulkan SDK 1.0.65.1 - 1.2.135.0
    * Installed to the default directory
    * $(VulkanDir) = C:\VulkanSDK\$(VulkanVer)\
        * Note: If you have installed Vulkan somewhere else (or a different version),
        * change the $(VulkanDir) and $(VulkanVer) Properties in the solution .props
* GLM
    * Installed as part of the Vulkan SDK
    * $(VulkanDir)Third-Party\Include\glm
* SDL2
    * Installed as part of the Vulkan SDK
    * $(VulkanDir)Third-Party\Include\SDL2

### Included

The following are included with the engine.

* [Microsoft/GSL](https://github.com/Microsoft/GSL)
    * Downloaded at release [3.0.1](https://github.com/microsoft/GSL/releases/tag/v3.0.1)
    * The contents of $\GSL\include\ were moved to $(SolutionDir)Third-Party\Include
    * The GSL.natvis file was added to the solution
* [googletest](https://github.com/google/googletest)
    * Included as a git submodule - tracking `v1.10.x` branch
* [GLFW](http://www.glfw.org/)
    * Included as a git submodule - tracking `latest` release branch

#### Updating Dependencies

Both googletest and GLFW are built from source, and then have their libs and include files manually moved into the appropriate folders.

* `Third-Party\Include\` - Header files
* `Third-Party\[Lib|Lib32]\[Debug|Release]\` - Built libs

They are both built using cmake, instructions are available in their repositories, or, a summary is:

* Have cmake
    * we used cmake-3.17.3-win64-x64.msi
    * Added to PATH
* In each project
    * Build 64 bit libs in a new folder `build`
        * `cmake .. -DBUILD_SHARED_LIBS=ON -G "Visual Studio 16 2019"`
        * Open and build in Visual Studio, using both Release and Debug
        * Copy the files to the right location
            * Note: these end up in different locations in different projects.
                * In glfw they are in src/Debug and src/Release
                * In gtest you need to copy contents of both the Bin and Lib folders
    * 32 bit builds are done the same way inside `build32`
        * add `-A Win32` when invoking cmake
    * Copy the include folder to PlayPhys

## MSVC Requirements

Our targets, at the moment, are:

* Debug, Win32
* Release, Win32
* Debug, x64
* Release, x64

### Project Options

We apply all project options automatically to all VS projects in the directory structure, using the new [Directory.Build.props system](https://docs.microsoft.com/en-us/visualstudio/msbuild/customize-your-build).

Using the [Vulkan Tutorial](https://vulkan-tutorial.com/Development_environment) as a guide, these apply to all projects.

Note: our flags for warnings and code analysis are rather aggressive. Code analysis in the unit test project is therefore disabled.

## Conventions

The code style is not entirely consistent yet, but the style guide is:

```c++
void functionName()
{
    auto local_variable = 0;
}

namespace NamespaceName
{
    class ClassName
    {
    public:
        bool memberFunction();

    private:
        int member_variable;
    }

    staticFunction();
}
```

i.e. all namespaces and variables are lower_case, all functions are camelCase, and all classes are PascalCase.

Other preferences include:

* Attempt to follow the [CppCoreGuidelines](https://github.com/isocpp/CppCoreGuidelines/blob/master/CppCoreGuidelines.md) where possible
* This requires utilising the `gsl`
    * Liberally use `Expects()` and `Ensures()` for pre- and post-conditions.
* Mark everything `noexcept` and `const` by default, and then remove qualifiers as they become invalid
* Prefer structs or `tuple` for multi-value returns, instead of out-params
    * If out-params are necessary, use a non-`const &`
    * In fact, non-`const &` should only be used for out-params
* Prefer numeric types defined in `Utils/Types.h` than variable-size types such as `long`.
    * They are simply `int8` instead of `int8_t`, etc.

## TODO

Project level TODOs include:

1. Create a "Third-Party" solution/script file which can compile all requirements and output to the correct folders.
1. Move to an Exe/Engine/App structure, where each loads the next as a .dll (following CMuratori's Handmade Hero hotloading method).
    * There may be some serious complications with GLFW doing this, perhaps we should switch to our own platform layer implementation, even though we want to avoid this (for now)?
    * Should the Exe or the Engine have a .pch to load third party dependencies? Perhaps from some config system?
1. Investigate how we can utilise Clang/LLVM sanitisers/static analysis/etc.
